﻿using StudentDAL;
using StudentModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentBLL
{
    public class AdminBll
    {
        /// <summary>
        /// 管理员登录
        /// </summary>
        /// <param name="account">账号</param>
        /// <param name="pwd">密码</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Admin> Login(string account, string pwd)
        {
            return AdminDal.Login(account, pwd);
        }
        /// <summary>
        /// 查询班级数量
        /// </summary>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Class> Sel_Class(string name)
        {
            return AdminDal.Sel_Class(name);
        }
        /// <summary>
        /// 查询教师数量
        /// </summary>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Teacher> Sel_Tea()
        {
            return AdminDal.Sel_Tea();
        }
        /// <summary>
        /// 查询教师信息
        /// </summary>
        /// <param name="name">教师姓名(为空，查询全表)</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Teacher> Sel_Teacher(string name)
        {
            return AdminDal.Sel_Teacher(name);
        }
        /// <summary>
        /// 查询科目信息
        /// </summary>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Subject> Sel_Subject(string name)
        {
            return AdminDal.Sel_Subject(name);
        }
        /// <summary>
        /// 查询某一个科目的信息
        /// </summary>
        /// <param name="id">科目编号</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Subject> Sel_Subject(int id)
        {
            return AdminDal.Sel_Subject(id);
        }
        /// <summary>
        /// 新增or修改科目
        /// </summary>
        /// <param name="id">科目编号</param>
        /// <param name="FsSub_Name">科目名称</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Teacher> Subject_Add(int? id, string FsSub_Name)
        {
            return AdminDal.Subject_Add(id, FsSub_Name);
        }
        /// <summary>
        /// 删除科目
        /// </summary>
        /// <param name="id">科目编号</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Teacher> Subject_Del(int id)
        {
            return AdminDal.Subject_Del(id);
        }
        /// <summary>
        /// 查询教师信息（编号）
        /// </summary>
        /// <param name="id">教师编号</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Teacher> Sel_Teacher(int id)
        {
            return AdminDal.Sel_Teacher(id);
        }
        /// <summary>
        /// 查询学生
        /// </summary>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Student> Student(string name)
        {
            return AdminDal.Student(name);
        }
        /// <summary>
        /// 删除教师
        /// </summary>
        /// <param name="id">教师编号</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Teacher> Teacher_Del(int id)
        {
            return AdminDal.Teacher_Del(id);
        }
        /// <summary>
        /// 添加教师
        /// </summary>
        /// <param name="FnSub_ID">科目编号</param>
        /// <param name="FnCla_ID">班级编号</param>
        /// <param name="FsTea_Account">账号</param>
        /// <param name="FsTea_Psw">密码</param>
        /// <param name="FsTea_Name">姓名</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Teacher> Tea_Add(int FnTea_ID, int FnSub_ID, int FnCla_ID, string FsTea_Account, string FsTea_Psw, string FsTea_Name)
        {
            return AdminDal.Tea_Add(FnTea_ID, FnSub_ID, FnCla_ID, FsTea_Account, FsTea_Psw, FsTea_Name);
        }
        /// <summary>
        /// 查询某一个班级的信息
        /// </summary>
        /// <param name="id">班级编号</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Class> Sel_Class(int id)
        {
            return AdminDal.Sel_Class(id);
        }
        /// <summary>
        /// 新增or修改班级
        /// </summary>
        /// <param name="id">班级编号</param>
        /// <param name="FsSub_Name">班级名称</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Class> Class_Add(int? id, string FsCla_Name)
        {
            return AdminDal.Class_Add(id, FsCla_Name);
        }
        /// <summary>
        /// 删除班级
        /// </summary>
        /// <param name="id">班级编号</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Class> Class_Del(int id)
        {
            return AdminDal.Class_Del(id);
        }
    }
}
