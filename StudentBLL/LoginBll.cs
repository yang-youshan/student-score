﻿using StudentModel;
using StudentDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentBLL
{
    public class LoginBll
    {
        public static SvcResult<TSTU_B_Student> Login_stu(string account, string pwd)
        {
            return LoginDal.Login_stu(account, pwd);
        }
        /// <summary>
        /// 查询班级（根据id查）
        /// </summary>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Class> Sel_ClassId(int id)
        {
            return LoginDal.Sel_ClassId(id);
        }
        /// 查询班级总人数
        /// </summary>
        /// <param name="id">班级id</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Class> Sel_ClassSum(int id)
        {
            return LoginDal.Sel_ClassSum(id);
        }
        /// <summary>
        /// 查询班级科目数
        /// </summary>
        /// <param name="id">科目id</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Class> Sel_ClassSubjectSum(int id)
        {
            return LoginDal.Sel_ClassSubjectSum(id);
        }
        /// <summary>
        /// 查询个人成绩
        /// </summary>
        /// <param name="id">学生编号</param>
        /// <param name="FnCla_ID">班级编号</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Student> Personal(int id, int FnCla_ID)
        {
            return LoginDal.Personal(id,FnCla_ID);
        }
        /// <summary>
        /// 各科成绩
        /// </summary>
        /// <param name="id">科目编号</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Student> Subject_Result(int? id)
        {
            return LoginDal.Subject_Result(id);
        }
        /// <summary>
        /// 总分查询
        /// </summary>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Score> Result(string name)
        {
            return LoginDal.Result(name);
        }
        /// <summary>
        /// 重置密码
        /// </summary>
        /// <param name="Account">账号</param>
        /// <param name="type">账号类型（1：学生，2：教师，3：管理员）</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Student> ForgotPassword(string Account, int? type)
        {
            return LoginDal.ForgotPassword(Account,type);
        }
    }
}
