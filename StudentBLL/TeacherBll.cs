﻿using StudentDAL;
using StudentModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentBLL
{
    public class TeacherBll
    {
        /// <summary>
        /// 教师登录
        /// </summary>
        /// <param name="account">账号</param>
        /// <param name="pwd">密码</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Teacher> Login(string account, string pwd)
        {
            return TeacherDal.Login(account, pwd);
        }
        /// <summary>
        /// 查询班级人员
        /// </summary>
        /// <param name="id">班级编号</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Student> Student(int id, string name)
        {
            return TeacherDal.Student(id,name);
        } 
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cid">班级编号</param>
        /// <param name="sid">科目编号</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Student> Sel_score(int cid, int sid)
        {
            return TeacherDal.Sel_score(cid,sid);
        }
        /// <summary>
        /// 录分----录入
        /// </summary>
        /// <param name="score">成绩</param>
        /// <param name="subid">科目编号</param>
        /// <param name="stuid">学生编号</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Score> Score_Add(double score, int subid, int stuid)
        {
            return TeacherDal.Score_Add(score, subid, stuid);
        }
        /// <summary>
        /// 查询班级
        /// </summary>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Class> Sel_Class()
        {
            return TeacherDal.Sel_Class();
        }
        /// <summary>
        /// 添加学生
        /// </summary>
        /// <param name="FsStu_Account">账号</param>
        /// <param name="FsStu_Name">姓名</param>
        /// <param name="FsStu_Psw">密码</param>
        /// <param name="FnCla_ID">班级编号</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Student> StudentAdd(string FsStu_Account, string FsStu_Name, string FsStu_Psw, int FnCla_ID)
        {
            return TeacherDal.StudentAdd(FsStu_Account, FsStu_Name, FsStu_Psw, FnCla_ID);
        }
        /// <summary>
        /// 删除学生
        /// </summary>
        /// <param name="id">学生编号</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Student> StudentDel(int id)
        {
            return TeacherDal.StudentDel(id);
        }
        /// <summary>
        /// 查询某个学生信息
        /// </summary>
        /// <param name="id">学生编号</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Student> SelStudent(int id)
        {
            return TeacherDal.SelStudent(id);
        }
        /// <summary>
        /// 修改学生信息
        /// </summary>
        /// <param name="id">学生编号</param>
        /// <param name="FsStu_Account">账号</param>
        /// <param name="FsStu_Name">姓名</param>
        /// <param name="FsStu_Psw">密码</param>
        /// <param name="FnCla_ID">班级编号</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Student> StudentEdit(int? id, string FsStu_Account, string FsStu_Name, string FsStu_Psw, int FnCla_ID)
        {
            return TeacherDal.StudentEdit(id,FsStu_Account, FsStu_Name, FsStu_Psw, FnCla_ID);
        }
        /// <summary>
        /// 总分详情
        /// </summary>
        /// <param name="id">学生编号</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Student> Sum_detil(int id)
        {
            return TeacherDal.Sum_detil(id);
        }
        /// <summary>
        /// 查询科目
        /// </summary>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Subject> Sel_Subject()
        {
            return TeacherDal.Sel_Subject();
        }
    }
}
