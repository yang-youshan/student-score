﻿using StudentModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentDAL
{
    public class AdminDal
    {
        /// <summary>
        /// 管理员登录
        /// </summary>
        /// <param name="account">账号</param>
        /// <param name="pwd">密码</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Admin> Login(string account, string pwd)
        {
            var result = new SvcResult<TSTU_B_Admin>();
            string _Name = null, _Pwd = null; int _ID = 0;
            var admin = new TSTU_B_Admin();
            var db = new dbHelper(dbHelper.EnumServer.Local)
            {
                DataBase = "StudentDB",
                execType = dbHelper.ExecType.IfRead,
                FuncError = ex =>
                {
                    //日志
                    result.Error = "系统内部错误";
                }
            };
            db.CmdText = "SELECT FnAdm_ID, FsAdm_Name, FsAdm_Account, FsAdm_Psw FROM StudentDB.dbo.TSTU_B_Admin WHERE FsAdm_Account=@FsAdm_Account";
            db.AddParameter("@FsAdm_Account", System.Data.SqlDbType.VarChar, account, 32);
            db.FuncRead = sdr =>
            {
                _Name = sdr["FsAdm_Name"].ToString();
                _Pwd = sdr["FsAdm_Psw"].ToString();
                _ID = int.Parse(sdr["FnAdm_ID"].ToString());
            };
            db.Exec();
            if (result.Error != null) return result;
            if (!db.Result)
            {
                result.Error = "用户不存在";
                return result;
            }
            if (pwd != _Pwd)
            {
                result.Error = "密码错误";
                return result;
            }
            admin.FnAdm_ID = _ID;
            admin.FsAdm_Account = account;
            admin.FsAdm_Name = _Name;
            admin.FsAdm_Psw = _Pwd;
            result.Obj = admin;
            return result;
        }
        /// <summary>
        /// 查询班级数量
        /// </summary>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Class> Sel_Class(string name)
        {
            var result = new SvcResult<TSTU_B_Class>();
            var db = new dbHelper(dbHelper.EnumServer.Local)
            {
                DataBase = "StudentDB",
                execType = dbHelper.ExecType.DataTable,
                FuncError = ex =>
                {
                    //日志
                    result.Error = "系统内部错误";
                }
            };
            if (string.IsNullOrWhiteSpace(name))
            {
                db.CmdText = "SELECT COUNT(*) OVER() AS cnt, ROW_NUMBER() OVER(ORDER BY FsCla_Name) AS rownumber, FnCla_ID, FsCla_Name FROM StudentDB.dbo.TSTU_B_Class";
            }
            else
            {
                db.CmdText = "SELECT COUNT(*) OVER() AS cnt, ROW_NUMBER() OVER(ORDER BY FsCla_Name) AS rownumber, FnCla_ID, FsCla_Name FROM StudentDB.dbo.TSTU_B_Class WHERE FsCla_Name LIKE '%'+@FsCla_Name+'%'";
                db.AddParameter("@FsCla_Name", System.Data.SqlDbType.NVarChar, name, 32);
            }
            db.Exec();
            DataTable dt = db.Result;
            List<TSTU_B_Class> objs = new List<TSTU_B_Class>(); //objs相当于数据表ProductClass
            TSTU_B_Class cla = new TSTU_B_Class();
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    //将当前行转换为具体的实体对象（obj相当行）
                    TSTU_B_Class obj = new TSTU_B_Class();
                    //obj实体的属性就等于当前行某列的值（属性相当于列）
                    obj.FsCla_Name = row["FsCla_Name"].ToString();
                    obj.FnCla_ID = int.Parse(row["FnCla_ID"].ToString());
                    obj.count = int.Parse(row["cnt"].ToString());
                    obj.rownumber = int.Parse(row["rownumber"].ToString());
                    cla.count = int.Parse(row["cnt"].ToString());
                    //将obj（行）添加到objs（表）
                    objs.Add(obj);
                }
            }
            //将当前行转换为具体的实体对象（obj相当行）
            result.ObjList = objs;
            result.Obj = cla;
            return result;
        }
        /// <summary>
        /// 查询教师数量
        /// </summary>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Teacher> Sel_Tea()
        {
            var result = new SvcResult<TSTU_B_Teacher>();
            var db = new dbHelper(dbHelper.EnumServer.Local)
            {
                DataBase = "StudentDB",
                execType = dbHelper.ExecType.DataTable,
                FuncError = ex =>
                {
                    //日志
                    result.Error = "系统内部错误";
                }
            };
            db.CmdText = "SELECT COUNT(*) OVER() AS cnt, FsTea_Psw, FnCla_ID, FsTea_Account, FsTea_Name, FnSub_ID, FnTea_ID FROM StudentDB.dbo.TSTU_B_Teacher ORDER BY FnCla_ID,FsTea_Name";
            db.Exec();
            DataTable dt = db.Result;
            List<TSTU_B_Teacher> objs = new List<TSTU_B_Teacher>(); //objs相当于数据表ProductClass
            TSTU_B_Teacher tea = new TSTU_B_Teacher();
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    //将当前行转换为具体的实体对象（obj相当行）
                    TSTU_B_Teacher obj = new TSTU_B_Teacher();
                    //obj实体的属性就等于当前行某列的值（属性相当于列）
                    obj.FsTea_Account = row["FsTea_Account"].ToString();
                    obj.FsTea_Name = row["FsTea_Name"].ToString();
                    obj.FsTea_Psw = row["FsTea_Psw"].ToString();
                    obj.FnCla_ID = int.Parse(row["FnCla_ID"].ToString());
                    obj.FnTea_ID = int.Parse(row["FnTea_ID"].ToString());
                    obj.FnSub_ID = int.Parse(row["FnSub_ID"].ToString());
                    tea.count = int.Parse(row["cnt"].ToString());
                    //将obj（行）添加到objs（表）
                    objs.Add(obj);
                }
            }
            //将当前行转换为具体的实体对象（obj相当行）
            result.ObjList = objs;
            result.Obj = tea;
            return result;
        }
        /// <summary>
        /// 查询教师信息
        /// </summary>
        /// <param name="name">教师姓名(为空，查询全表)</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Teacher> Sel_Teacher(string name)
        {
            var result = new SvcResult<TSTU_B_Teacher>();

            var db = new dbHelper(dbHelper.EnumServer.Local)
            {
                DataBase = "StudentDB",
                execType = dbHelper.ExecType.DataTable,
                FuncError = ex =>
                {
                    //日志
                    result.Error = "系统内部错误";
                }
            };
            if (string.IsNullOrWhiteSpace(name))
            {
                db.CmdText = "SELECT  ROW_NUMBER() OVER(ORDER BY T1.FnCla_ID) AS rownumber, T1.FnCla_ID, T1.FnSub_ID ,T1.FnTea_ID ,T1.FsTea_Account, T1.FsTea_Name ,T1.FsTea_Psw ,T2.FsCla_Name ,T3.FsSub_Name FROM StudentDB.dbo.TSTU_B_Teacher T1 INNER JOIN StudentDB.dbo.TSTU_B_Class T2 ON(T1.FnCla_ID=T2.FnCla_ID) INNER JOIN StudentDB.dbo.TSTU_B_Subject T3 ON(T1.FnSub_ID=T3.FnSub_ID)";
            }
            else
            {
                db.CmdText = "SELECT  ROW_NUMBER() OVER(ORDER BY T1.FnCla_ID) AS rownumber, T1.FnCla_ID, T1.FnSub_ID ,T1.FnTea_ID ,T1.FsTea_Account, T1.FsTea_Name ,T1.FsTea_Psw ,T2.FsCla_Name ,T3.FsSub_Name FROM StudentDB.dbo.TSTU_B_Teacher T1 INNER JOIN StudentDB.dbo.TSTU_B_Class T2 ON(T1.FnCla_ID=T2.FnCla_ID) INNER JOIN StudentDB.dbo.TSTU_B_Subject T3 ON(T1.FnSub_ID=T3.FnSub_ID) WHERE T1.FsTea_Name LIKE '%'+@FsTea_Name+'%'";
                db.AddParameter("@FsTea_Name", System.Data.SqlDbType.NVarChar, name,32);

            }
            db.Exec();
            DataTable dt = db.Result;
            List<TSTU_B_Teacher> objs = new List<TSTU_B_Teacher>(); //objs相当于数据表ProductClass
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    //将当前行转换为具体的实体对象（obj相当行）
                    TSTU_B_Teacher obj = new TSTU_B_Teacher();
                    //obj实体的属性就等于当前行某列的值（属性相当于列）
                    obj.FsTea_Account = row["FsTea_Account"].ToString();
                    obj.FsTea_Name = row["FsTea_Name"].ToString();
                    obj.FsSub_Name = row["FsSub_Name"].ToString();
                    obj.FsTea_Psw = row["FsTea_Psw"].ToString();
                    obj.FsCla_Name = row["FsCla_Name"].ToString();
                    obj.FnTea_ID = int.Parse(row["FnTea_ID"].ToString());
                    obj.FnSub_ID = int.Parse(row["FnSub_ID"].ToString());
                    obj.FnCla_ID = int.Parse(row["FnCla_ID"].ToString());
                    obj.rownumber = int.Parse(row["rownumber"].ToString());
                    //将obj（行）添加到objs（表）
                    objs.Add(obj);
                }
            }
            //将当前行转换为具体的实体对象（obj相当行）
            result.ObjList = objs;
            return result;
        }
        /// <summary>
        /// 查询教师信息（编号）
        /// </summary>
        /// <param name="id">教师编号</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Teacher> Sel_Teacher(int id)
        {
            var result = new SvcResult<TSTU_B_Teacher>();

            var db = new dbHelper(dbHelper.EnumServer.Local)
            {
                DataBase = "StudentDB",
                execType = dbHelper.ExecType.DataTable,
                FuncError = ex =>
                {
                    //日志
                    result.Error = "系统内部错误";
                }
            };

            db.CmdText = "SELECT FnCla_ID, FnSub_ID, FnTea_ID, FsTea_Account, FsTea_Name ,FsTea_Psw FROM StudentDB.dbo.TSTU_B_Teacher  WHERE FnTea_ID=@FnTea_ID";
            db.AddParameter("@FnTea_ID", System.Data.SqlDbType.Int, id);

            db.Exec();
            DataTable dt = db.Result;
            List<TSTU_B_Teacher> objs = new List<TSTU_B_Teacher>(); //objs相当于数据表ProductClass
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    //将当前行转换为具体的实体对象（obj相当行）
                    TSTU_B_Teacher obj = new TSTU_B_Teacher();
                    //obj实体的属性就等于当前行某列的值（属性相当于列）
                    obj.FsTea_Account = row["FsTea_Account"].ToString();
                    obj.FsTea_Name = row["FsTea_Name"].ToString();
                    obj.FsTea_Psw = row["FsTea_Psw"].ToString();
                    obj.FnTea_ID = int.Parse(row["FnTea_ID"].ToString());
                    obj.FnSub_ID = int.Parse(row["FnSub_ID"].ToString());
                    obj.FnCla_ID = int.Parse(row["FnCla_ID"].ToString());
                    //将obj（行）添加到objs（表）
                    objs.Add(obj);
                }
            }
            //将当前行转换为具体的实体对象（obj相当行）
            result.ObjList = objs;
            return result;
        }
        /// <summary>
        /// 删除教师
        /// </summary>
        /// <param name="id">教师编号</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Teacher> Teacher_Del(int id)
        {

            var result = new SvcResult<TSTU_B_Teacher>();

            if (id == 0)
            {
                result.Error = "数据错误,教师编号为空";
            }
            if (result.Error != null) return result;
            var db = new dbHelper(dbHelper.EnumServer.Local)
            {
                DataBase = "StudentDB",
                execType = dbHelper.ExecType.Exec,
                FuncError = ex =>
                {
                    //日志
                    result.Error = "系统内部错误";
                }
            };
            db.CmdText = "StudentDB.dbo.PR_PRC_TeacherDel";
            db.IsProc = true;
            db.AddParameter("@FnTea_ID", System.Data.SqlDbType.Int, id);
            db.NeedOutError = true;
            db.Exec();
            result.Error = db.OutParameters["Error"].ToString();
            return result;
        }
        /// <summary>
        /// 添加教师
        /// </summary>
        /// <param name="FnSub_ID">科目编号</param>
        /// <param name="FnCla_ID">班级编号</param>
        /// <param name="FsTea_Account">账号</param>
        /// <param name="FsTea_Psw">密码</param>
        /// <param name="FsTea_Name">姓名</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Teacher> Tea_Add(int FnTea_ID, int FnSub_ID, int FnCla_ID, string FsTea_Account, string FsTea_Psw, string FsTea_Name)
        {
            var result = new SvcResult<TSTU_B_Teacher>();
            if (FnSub_ID == 0 || FnCla_ID == 0 || string.IsNullOrWhiteSpace(FsTea_Account) || string.IsNullOrWhiteSpace(FsTea_Psw) || string.IsNullOrWhiteSpace(FsTea_Name))
            {
                result.Error = "数据错误(存在空值)";
                return result;
            }
            if (result.Error != null) return result;
            var db = new dbHelper(dbHelper.EnumServer.Local)
            {
                DataBase = "StudentDB",
                execType = dbHelper.ExecType.Exec,
                FuncError = ex =>
                {
                    //日志
                    result.Error = "系统内部错误";
                }
            };
            db.CmdText = "StudentDB.dbo.PR_PRC_TeacherAdd";
            db.IsProc = true;
            db.AddParameter("@FnTea_ID", System.Data.SqlDbType.Int, FnTea_ID);
            db.AddParameter("@FnSub_ID", System.Data.SqlDbType.Int, FnSub_ID);
            db.AddParameter("@FnCla_ID", System.Data.SqlDbType.Int, FnCla_ID);
            db.AddParameter("@FsTea_Account", System.Data.SqlDbType.VarChar, FsTea_Account, 32);
            db.AddParameter("@FsTea_Psw", System.Data.SqlDbType.VarChar, FsTea_Psw, 32);
            db.AddParameter("@FsTea_Name", System.Data.SqlDbType.NVarChar, FsTea_Name, 32);
            db.NeedOutError = true;
            db.Exec();
            result.Error = db.OutParameters["Error"].ToString();
            if (db.Result == true)
            {
                result.ErrorCode = 1;//执行成功
                result.Error = "执行成功";
            }
            return result;
        }
        /// <summary>
        /// 查询科目信息
        /// </summary>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Subject> Sel_Subject(string name)
        {
            var result = new SvcResult<TSTU_B_Subject>();

            var db = new dbHelper(dbHelper.EnumServer.Local)
            {
                DataBase = "StudentDB",
                execType = dbHelper.ExecType.DataTable,
                FuncError = ex =>
                {
                    //日志
                    result.Error = "系统内部错误";
                }
            };
            if (string.IsNullOrWhiteSpace(name))
            {
                db.CmdText = "SELECT  ROW_NUMBER() OVER(ORDER BY FsSub_Name) AS rownumber, FnSub_ID ,FsSub_Name FROM  StudentDB.dbo.TSTU_B_Subject";
            }
            else
            {
                db.CmdText = "SELECT  ROW_NUMBER() OVER(ORDER BY FsSub_Name) AS rownumber, FnSub_ID ,FsSub_Name FROM  StudentDB.dbo.TSTU_B_Subject WHERE FsSub_Name LIKE '%'+@FsSub_Name+'%'";
                db.AddParameter("@FsSub_Name", System.Data.SqlDbType.NVarChar, name,32);
            }
            

            db.Exec();
            DataTable dt = db.Result;
            List<TSTU_B_Subject> objs = new List<TSTU_B_Subject>(); //objs相当于数据表ProductClass
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    //将当前行转换为具体的实体对象（obj相当行）
                    TSTU_B_Subject obj = new TSTU_B_Subject();
                    //obj实体的属性就等于当前行某列的值（属性相当于列）
                    obj.FsSub_Name = row["FsSub_Name"].ToString();
                    obj.FnSub_ID = int.Parse(row["FnSub_ID"].ToString());
                    obj.rownumber = int.Parse(row["rownumber"].ToString());
                    //将obj（行）添加到objs（表）
                    objs.Add(obj);
                }
            }
            //将当前行转换为具体的实体对象（obj相当行）
            result.ObjList = objs;
            return result;
        }
        /// <summary>
        /// 查询某一个科目的信息
        /// </summary>
        /// <param name="id">科目编号</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Subject> Sel_Subject(int id)
        {
            var result = new SvcResult<TSTU_B_Subject>();

            var db = new dbHelper(dbHelper.EnumServer.Local)
            {
                DataBase = "StudentDB",
                execType = dbHelper.ExecType.DataTable,
                FuncError = ex =>
                {
                    //日志
                    result.Error = "系统内部错误";
                }
            };
            db.CmdText = "SELECT FnSub_ID ,FsSub_Name FROM StudentDB.dbo.TSTU_B_Subject WHERE FnSub_ID=@FnSub_ID";
            db.AddParameter("@FnSub_ID", System.Data.SqlDbType.Int, id);
            db.Exec();
            DataTable dt = db.Result;
            List<TSTU_B_Subject> objs = new List<TSTU_B_Subject>(); //objs相当于数据表ProductClass
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    //将当前行转换为具体的实体对象（obj相当行）
                    TSTU_B_Subject obj = new TSTU_B_Subject();
                    //obj实体的属性就等于当前行某列的值（属性相当于列）
                    obj.FsSub_Name = row["FsSub_Name"].ToString();
                    obj.FnSub_ID = int.Parse(row["FnSub_ID"].ToString());
                    //将obj（行）添加到objs（表）
                    objs.Add(obj);
                }
            }
            //将当前行转换为具体的实体对象（obj相当行）
            result.ObjList = objs;
            return result;
        }
        /// <summary>
        /// 新增or修改科目
        /// </summary>
        /// <param name="id">科目编号</param>
        /// <param name="FsSub_Name">科目名称</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Teacher> Subject_Add(int? id, string FsSub_Name)
        {
            var result = new SvcResult<TSTU_B_Teacher>();
            if (string.IsNullOrWhiteSpace(FsSub_Name))
            {
                result.Error = "数据错误(存在空值)";
                return result;
            }
            if (result.Error != null) return result;
            var db = new dbHelper(dbHelper.EnumServer.Local)
            {
                DataBase = "StudentDB",
                execType = dbHelper.ExecType.Exec,
                FuncError = ex =>
                {
                    //日志
                    result.Error = "系统内部错误";
                }
            };
            db.CmdText = "StudentDB.dbo.PR_PRC_SubjectAdd_UP";
            db.IsProc = true;
            db.AddParameter("@FnSub_ID", System.Data.SqlDbType.Int, id);
            db.AddParameter("@FsSub_Name", System.Data.SqlDbType.NVarChar, FsSub_Name, 32);
            db.NeedOutError = true;
            db.Exec();
            result.Error = db.OutParameters["Error"].ToString();
            return result;
        }
        /// <summary>
        /// 删除科目
        /// </summary>
        /// <param name="id">科目编号</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Teacher> Subject_Del(int id)
        {
            var result = new SvcResult<TSTU_B_Teacher>();
            if (id == 0)
            {
                result.Error = "数据错误(存在空值)";
                return result;
            }
            if (result.Error != null) return result;
            var db = new dbHelper(dbHelper.EnumServer.Local)
            {
                DataBase = "StudentDB",
                execType = dbHelper.ExecType.Exec,
                FuncError = ex =>
                {
                    //日志
                    result.Error = "系统内部错误";
                }
            };

            db.CmdText = "DELETE FROM StudentDB.dbo.TSTU_B_Subject WHERE FnSub_ID=@FnSub_ID";
            db.AddParameter("@FnSub_ID", System.Data.SqlDbType.Int, id);

            db.Exec();
            if (db.Result == true)
            {
                result.ErrorCode = 1;//执行成功
                result.Error = "执行成功";
            }
            return result;
        }
        /// <summary>
        /// 查询某一个班级的信息
        /// </summary>
        /// <param name="id">班级编号</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Class> Sel_Class(int id)
        {
            var result = new SvcResult<TSTU_B_Class>();

            var db = new dbHelper(dbHelper.EnumServer.Local)
            {
                DataBase = "StudentDB",
                execType = dbHelper.ExecType.DataTable,
                FuncError = ex =>
                {
                    //日志
                    result.Error = "系统内部错误";
                }
            };
            db.CmdText = "SELECT FnCla_ID, FsCla_Name FROM StudentDB.dbo.TSTU_B_Class WHERE FnCla_ID=@FnCla_ID";
            db.AddParameter("@FnCla_ID", System.Data.SqlDbType.Int, id);
            db.Exec();
            DataTable dt = db.Result;
            List<TSTU_B_Class> objs = new List<TSTU_B_Class>(); //objs相当于数据表ProductClass
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    //将当前行转换为具体的实体对象（obj相当行）
                    TSTU_B_Class obj = new TSTU_B_Class();
                    //obj实体的属性就等于当前行某列的值（属性相当于列）
                    obj.FsCla_Name = row["FsCla_Name"].ToString();
                    obj.FnCla_ID = int.Parse(row["FnCla_ID"].ToString());
                    //将obj（行）添加到objs（表）
                    objs.Add(obj);
                }
            }
            //将当前行转换为具体的实体对象（obj相当行）
            result.ObjList = objs;
            return result;
        }
        /// <summary>
        /// 新增or修改班级
        /// </summary>
        /// <param name="id">班级编号</param>
        /// <param name="FsSub_Name">班级名称</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Class> Class_Add(int? id, string FsCla_Name)
        {
            var result = new SvcResult<TSTU_B_Class>();
            if (string.IsNullOrWhiteSpace(FsCla_Name))
            {
                result.Error = "数据错误(存在空值)";
                return result;
            }
            if (result.Error != null) return result;
            var db = new dbHelper(dbHelper.EnumServer.Local)
            {
                DataBase = "StudentDB",
                execType = dbHelper.ExecType.Exec,
                FuncError = ex =>
                {
                    //日志
                    result.Error = "系统内部错误";
                }
            };
            db.CmdText = "StudentDB.dbo.PR_PRC_ClassAdd_UP";
            db.IsProc = true;
            db.AddParameter("@FnCla_ID", System.Data.SqlDbType.Int, id);
            db.AddParameter("@FsCla_Name", System.Data.SqlDbType.NVarChar, FsCla_Name, 32);
            db.NeedOutError = true;
            db.Exec();
            result.Error = db.OutParameters["Error"].ToString();
            return result;
        }
        /// <summary>
        /// 删除班级
        /// </summary>
        /// <param name="id">班级编号</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Class> Class_Del(int id)
        {
            var result = new SvcResult<TSTU_B_Class>();
            if (id == 0)
            {
                result.Error = "数据错误(存在空值)";
                return result;
            }
            if (result.Error != null) return result;
            var db = new dbHelper(dbHelper.EnumServer.Local)
            {
                DataBase = "StudentDB",
                execType = dbHelper.ExecType.Exec,
                FuncError = ex =>
                {
                    //日志
                    result.Error = "系统内部错误";
                }
            };

            db.CmdText = "DELETE FROM StudentDB.dbo.TSTU_B_Class WHERE FnCla_ID=@FnCla_ID";
            db.AddParameter("@FnCla_ID", System.Data.SqlDbType.Int, id);

            db.Exec();
            if (db.Result == true)
            {
                result.ErrorCode = 1;//执行成功
                result.Error = "执行成功";
            }
            return result;
        }
        /// <summary>
        /// 查询学生(or模糊查询)
        /// </summary>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Student> Student(string name)
        {
            var result = new SvcResult<TSTU_B_Student>();

            var db = new dbHelper(dbHelper.EnumServer.Local)
            {
                DataBase = "StudentDB",
                execType = dbHelper.ExecType.DataTable,
                FuncError = ex =>
                {
                    //日志
                    result.Error = "系统内部错误";
                }
            };
            if (string.IsNullOrWhiteSpace(name))
            {
                db.CmdText = "SELECT ROW_NUMBER() OVER(ORDER BY T1.FnCla_ID,T2.FsStu_Name) AS rownumber, T1.FnCla_ID, T1.FsCla_Name, T2.FnStu_ID, T2.FsStu_Account, T2.FsStu_Name, T2.FsStu_Psw FROM StudentDB.dbo.TSTU_B_Class T1 INNER JOIN StudentDB.dbo.TSTU_B_Student T2 ON(T1.FnCla_ID=T2.FnCla_ID)";
            }
            else
            {
                db.CmdText = "SELECT ROW_NUMBER() OVER(ORDER BY T1.FnCla_ID, T2.FsStu_Name) AS rownumber, T1.FnCla_ID, T1.FsCla_Name, T2.FnStu_ID, T2.FsStu_Account, T2.FsStu_Name, T2.FsStu_Psw FROM StudentDB.dbo.TSTU_B_Class T1 INNER JOIN StudentDB.dbo.TSTU_B_Student T2 ON(T1.FnCla_ID = T2.FnCla_ID) WHERE T2.FsStu_Name LIKE '%'+@FsStu_Name+'%'";
                db.AddParameter("@FsStu_Name", System.Data.SqlDbType.NVarChar, name,32);
            }

            db.Exec();
            DataTable dt = db.Result;
            List<TSTU_B_Student> objs = new List<TSTU_B_Student>(); //objs相当于数据表ProductClass
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    //将当前行转换为具体的实体对象（obj相当行）
                    TSTU_B_Student obj = new TSTU_B_Student();
                    //obj实体的属性就等于当前行某列的值（属性相当于列）
                    obj.FsStu_Name = row["FsStu_Name"].ToString();
                    obj.FsStu_Account = row["FsStu_Account"].ToString();
                    obj.FsCla_Name = row["FsCla_Name"].ToString();
                    obj.rownumber = int.Parse(row["rownumber"].ToString());
                    obj.FnStu_ID = int.Parse(row["FnStu_ID"].ToString());
                    obj.FnCla_ID = int.Parse(row["FnCla_ID"].ToString());
                    //将obj（行）添加到objs（表）
                    objs.Add(obj);
                }
            }
            //将当前行转换为具体的实体对象（obj相当行）
            result.ObjList = objs;
            return result;
        }
    }
}
