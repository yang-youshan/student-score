﻿using StudentModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentDAL
{
    public class TeacherDal
    {
        /// <summary>
        /// 教师登录
        /// </summary>
        /// <param name="account">账号</param>
        /// <param name="pwd">密码</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Teacher> Login(string account, string pwd)
        {
            var result = new SvcResult<TSTU_B_Teacher>();
            string _Name = null, _Pwd = null; int _FnCla = 0, _ID = 0,_FnSub = 0;
             var tea = new TSTU_B_Teacher();
            var db = new dbHelper(dbHelper.EnumServer.Local)
            {
                DataBase = "StudentDB",
                execType = dbHelper.ExecType.IfRead,
                FuncError = ex =>
                {
                    //日志
                    result.Error = "系统内部错误";
                }
            };
            db.CmdText = "SELECT FnTea_ID, FnSub_ID, FnCla_ID, FsTea_Name, FsTea_Psw, FsTea_Account FROM StudentDB.dbo.TSTU_B_Teacher WHERE FsTea_Account=@FsTea_Account";
            db.AddParameter("@FsTea_Account", System.Data.SqlDbType.VarChar, account, 32);
            db.FuncRead = sdr =>
            {
                _Name = sdr["FsTea_Name"].ToString();
                _Pwd = sdr["FsTea_Psw"].ToString();
                _ID = int.Parse(sdr["FnTea_ID"].ToString());
                _FnCla = int.Parse(sdr["FnCla_ID"].ToString());
                _FnSub = int.Parse(sdr["FnSub_ID"].ToString());
            };
            db.Exec();
            if (result.Error != null) return result;
            if (!db.Result)
            {
                result.Error = "用户不存在";
                return result;
            }
            if (pwd != _Pwd)
            {
                result.Error = "密码错误";
                return result;
            }
            tea.FnCla_ID = _FnCla;
            tea.FnTea_ID = _ID;
            tea.FsTea_Name = _Name;
            tea.FsTea_Account = account;
            tea.FnSub_ID = _FnSub;
            tea.FsTea_Psw = _Pwd;
            result.Obj = tea;
            return result;
        }
        /// <summary>
        /// 查询班级人员
        /// </summary>
        /// <param name="id">班级编号</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Student> Student(int id,string name)
        {
            var result = new SvcResult<TSTU_B_Student>();

            var db = new dbHelper(dbHelper.EnumServer.Local)
            {
                DataBase = "StudentDB",
                execType = dbHelper.ExecType.DataTable,
                FuncError = ex =>
                {
                    //日志
                    result.Error = "系统内部错误";
                }
            };
            if (string.IsNullOrWhiteSpace(name))
            {
                db.CmdText = "SELECT ROW_NUMBER() OVER(ORDER BY T2.FnStu_ID) AS rownumber,T1.FnCla_ID, T1.FsCla_Name, T2.FnStu_ID, T2.FsStu_Account, T2.FsStu_Name, T2.FsStu_Psw FROM StudentDB.dbo.TSTU_B_Class T1 INNER JOIN StudentDB.dbo.TSTU_B_Student T2 ON(T1.FnCla_ID=T2.FnCla_ID) WHERE T1.FnCla_ID=@FnCla_ID";
                db.AddParameter("@FnCla_ID", System.Data.SqlDbType.Int, id);
            }
            else
            {
                db.CmdText = "SELECT ROW_NUMBER() OVER(ORDER BY T2.FnStu_ID) AS rownumber,T1.FnCla_ID, T1.FsCla_Name, T2.FnStu_ID, T2.FsStu_Account, T2.FsStu_Name, T2.FsStu_Psw FROM StudentDB.dbo.TSTU_B_Class T1 INNER JOIN StudentDB.dbo.TSTU_B_Student T2 ON(T1.FnCla_ID=T2.FnCla_ID) WHERE T1.FnCla_ID=@FnCla_ID AND T2.FsStu_Name LIKE '%'+@FsStu_Name+'%'";
                db.AddParameter("@FnCla_ID", System.Data.SqlDbType.Int, id);
                db.AddParameter("@FsStu_Name", System.Data.SqlDbType.NVarChar, name,32) ;
            }

            db.Exec();
            DataTable dt = db.Result;
            List<TSTU_B_Student> objs = new List<TSTU_B_Student>(); //objs相当于数据表ProductClass
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    //将当前行转换为具体的实体对象（obj相当行）
                    TSTU_B_Student obj = new TSTU_B_Student();
                    //obj实体的属性就等于当前行某列的值（属性相当于列）
                    obj.FsStu_Name = row["FsStu_Name"].ToString();
                    obj.FsStu_Account = row["FsStu_Account"].ToString();
                    obj.FsCla_Name = row["FsCla_Name"].ToString();
                    obj.rownumber = int.Parse(row["rownumber"].ToString());
                    obj.FnStu_ID = int.Parse(row["FnStu_ID"].ToString());
                    obj.FnCla_ID = int.Parse(row["FnCla_ID"].ToString());
                    //将obj（行）添加到objs（表）
                    objs.Add(obj);
                }
            }
            //将当前行转换为具体的实体对象（obj相当行）
            result.ObjList = objs;
            return result;
        }
        /// <summary>
        /// 录分---查询成绩
        /// </summary>
        /// <param name="cid">班级id</param>
        /// <param name="sid">科目id</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Student> Sel_score(int cid,int sid)
        {
            var result = new SvcResult<TSTU_B_Student>();

            var db = new dbHelper(dbHelper.EnumServer.Local)
            {
                DataBase = "StudentDB",
                execType = dbHelper.ExecType.DataTable,
                FuncError = ex =>
                {
                    //日志
                    result.Error = "系统内部错误";
                }
            };
            db.CmdText = "SELECT COUNT(*) OVER() AS cnt, ROW_NUMBER() OVER(ORDER BY CASE WHEN T2.FnSco_Result IS NULL THEN 1 ELSE 0 END DESC,T2.FnSco_Result DESC) AS rownumber, T1.FnStu_ID, T1.FsStu_Account, T1.FsStu_Name, T4.FsCla_Name, T3.FsSub_Name, T2.FnSco_Result From StudentDB.dbo.TSTU_B_Student T1 INNER JOIN StudentDB.dbo.TSTU_B_Score T2 ON(T1.FnStu_ID = T2.FnStu_ID) INNER JOIN StudentDB.dbo.TSTU_B_Subject T3 ON(T2.FnSub_ID = T3.FnSub_ID) INNER JOIN StudentDB.dbo.TSTU_B_Class T4 ON(T4.FnCla_ID = T1.FnCla_ID) WHERE T3.FnSub_ID = @FnSub_ID AND T4.FnCla_ID = @FnCla_ID";
            //db.CmdText = "SELECT * FROM  StudentDB.dbo.TSTU_B_Student T1 INNER JOIN StudentDB.dbo.TSTU_B_Score T2 ON (T1.FnStu_ID=T2.FnStu_ID) INNER JOIN StudentDB.dbo.TSTU_B_Subject su ON (T2.FnSub_ID=su.FnSub_ID) INNER JOIN StudentDB.dbo.TSTU_B_Class T4 ON (T4.FnCla_ID=T1.FnCla_ID) WHERE su.FnSub_ID=@FnSub_ID AND T4.FnCla_ID=@FnCla_ID ORDER BY T1.FnStu_ID";
            db.AddParameter("@FnSub_ID", System.Data.SqlDbType.Int, sid);
            db.AddParameter("@FnCla_ID", System.Data.SqlDbType.Int, cid);

            db.Exec();
            DataTable dt = db.Result;
            List<TSTU_B_Student> objs = new List<TSTU_B_Student>(); //objs相当于数据表ProductClass
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    //将当前行转换为具体的实体对象（obj相当行）
                    TSTU_B_Student obj = new TSTU_B_Student();
                    //obj实体的属性就等于当前行某列的值（属性相当于列）
                    obj.FsStu_Name = row["FsStu_Name"].ToString();
                    obj.FsStu_Account = row["FsStu_Account"].ToString();
                    obj.FsCla_Name = row["FsCla_Name"].ToString();
                    obj.FsSub_Name = row["FsSub_Name"].ToString();
                    obj.FnStu_ID = int.Parse(row["FnStu_ID"].ToString());
                    obj.rownumber = int.Parse(row["rownumber"].ToString());
                    if (!string.IsNullOrWhiteSpace(row["FnSco_Result"].ToString()))
                    {
                        obj.FnSco_Result = (decimal?)double.Parse(row["FnSco_Result"].ToString());
                    }
                    else
                    {
                        obj.FnSco_Result = null;
                    }
                    //将obj（行）添加到objs（表）
                    objs.Add(obj);
                }
            }
            //将当前行转换为具体的实体对象（obj相当行）
            result.ObjList = objs;
            return result;
        }
        /// <summary>
        /// 录分----录入
        /// </summary>
        /// <param name="score">成绩</param>
        /// <param name="subid">科目编号</param>
        /// <param name="stuid">学生编号</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Score> Score_Add(double score, int subid, int stuid)
        {
            var result = new SvcResult<TSTU_B_Score>();
            if (score == 0 || subid == 0 || stuid == 0)
            {
                result.Error = "数据错误";
            }
            if (score>100 || score<0)
            {
                result.Error = "分数范围0-100";
            }
            if (result.Error != null) return result;
            var db = new dbHelper(dbHelper.EnumServer.Local)
            {
                DataBase = "StudentDB",
                execType = dbHelper.ExecType.Exec,
                FuncError = ex =>
                {
                    //日志
                    result.Error = "系统内部错误";
                }
            };
            db.CmdText = "UPDATE StudentDB.dbo.TSTU_B_Score SET FnSco_Result = @FnSco_Result WHERE FnStu_ID=@FnStu_ID AND FnSub_ID=@FnSub_ID";
            db.AddParameter("@FnSco_Result", System.Data.SqlDbType.Decimal, score, 0, 10, 2);
            db.AddParameter("@FnStu_ID", System.Data.SqlDbType.Int, stuid);
            db.AddParameter("@FnSub_ID", System.Data.SqlDbType.Int, subid);
            db.Exec();
            if (db.Result==true)
            {
                result.ErrorCode = 1;//操作成功
                return result;
            }
            return result;
        }
        /// <summary>
        /// 查询班级
        /// </summary>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Class> Sel_Class()
        {
            var result = new SvcResult<TSTU_B_Class>();
            var db = new dbHelper(dbHelper.EnumServer.Local)
            {
                DataBase = "StudentDB",
                execType = dbHelper.ExecType.DataTable,
                FuncError = ex =>
                {
                    //日志
                    result.Error = "系统内部错误";
                }
            };
            db.CmdText = "SELECT FnCla_ID, FsCla_Name FROM StudentDB.dbo.TSTU_B_Class ORDER BY FsCla_Name";
            db.Exec();
            DataTable dt = db.Result;
            List<TSTU_B_Class> objs = new List<TSTU_B_Class>(); //objs相当于数据表ProductClass
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    //将当前行转换为具体的实体对象（obj相当行）
                    TSTU_B_Class obj = new TSTU_B_Class();
                    //obj实体的属性就等于当前行某列的值（属性相当于列）
                    obj.FsCla_Name = row["FsCla_Name"].ToString();
                    obj.FnCla_ID = int.Parse(row["FnCla_ID"].ToString());
                    //将obj（行）添加到objs（表）
                    objs.Add(obj);
                }
            }
            //将当前行转换为具体的实体对象（obj相当行）
            result.ObjList = objs;
            return result;
        }
        /// <summary>
        /// 添加学生
        /// </summary>
        /// <param name="FsStu_Account">账号</param>
        /// <param name="FsStu_Name">姓名</param>
        /// <param name="FsStu_Psw">密码</param>
        /// <param name="FnCla_ID">班级编号</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Student> StudentAdd(string FsStu_Account, string FsStu_Name, string FsStu_Psw, int FnCla_ID)
        {
            
            var result = new SvcResult<TSTU_B_Student>();
            if (string.IsNullOrWhiteSpace(FsStu_Account)||string.IsNullOrWhiteSpace(FsStu_Name)||string.IsNullOrWhiteSpace(FsStu_Psw))
            {
                result.Error = "数据错误";
            }
            if (FnCla_ID == 0)
            {
                result.Error = "数据错误(请补全信息)";
            }
            if (result.Error != null) return result;
            var db = new dbHelper(dbHelper.EnumServer.Local)
            {
                DataBase = "StudentDB",
                execType = dbHelper.ExecType.Exec,
                FuncError = ex =>
                {
                    //日志
                    result.Error = "系统内部错误";
                }
            };
            db.CmdText = "StudentDB.dbo.PR_PRC_StudentAdd";
            db.IsProc = true;
            db.AddParameter("@FnStu_ID", System.Data.SqlDbType.Int, 0);
            db.AddParameter("@FnCla_ID", System.Data.SqlDbType.Int, FnCla_ID);
            db.AddParameter("@FsStu_Account", System.Data.SqlDbType.VarChar, FsStu_Account, 32);
            db.AddParameter("@FsStu_Psw", System.Data.SqlDbType.VarChar, FsStu_Psw, 32);
            db.AddParameter("@FsStu_Name", System.Data.SqlDbType.NVarChar, FsStu_Name, 32);
            db.NeedOutError = true;
            db.Exec();
            result.Error = db.OutParameters["Error"].ToString();
            if (db.Result == true)
            {
                result.ErrorCode = 1;//操作成功
                return result;
            }
            return result;
        }
        /// <summary>
        /// 删除学生
        /// </summary>
        /// <param name="id">学生编号</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Student> StudentDel(int id)
        {

            var result = new SvcResult<TSTU_B_Student>();
            
            if (id == 0)
            {
                result.Error = "数据错误";
            }
            if (result.Error != null) return result;
            var db = new dbHelper(dbHelper.EnumServer.Local)
            {
                DataBase = "StudentDB",
                execType = dbHelper.ExecType.Exec,
                FuncError = ex =>
                {
                    //日志
                    result.Error = "系统内部错误";
                }
            };
            db.CmdText = "StudentDB.dbo.PR_PRC_StudentDel";
            db.IsProc = true;
            db.AddParameter("@FnStu_ID", System.Data.SqlDbType.Int, id);
            db.NeedOutError = true;
            db.Exec();
            result.Error = db.OutParameters["Error"].ToString();
            if (db.Result == true)
            {
                result.ErrorCode = 1;//操作成功
                return result;
            }
            return result;
        }
        /// <summary>
        /// 查询某个学生信息
        /// </summary>
        /// <param name="id">学生编号</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Student> SelStudent(int id)
        {

            var result = new SvcResult<TSTU_B_Student>();

            if (id == 0)
            {
                result.Error = "数据错误";
            }
            if (result.Error != null) return result;
            var db = new dbHelper(dbHelper.EnumServer.Local)
            {
                DataBase = "StudentDB",
                execType = dbHelper.ExecType.DataTable,
                FuncError = ex =>
                {
                    //日志
                    result.Error = "系统内部错误";
                }
            };
            db.CmdText = "SELECT FnStu_ID, FnCla_ID, FsStu_Account, FsStu_Psw, FsStu_Name FROM StudentDB.dbo.TSTU_B_Student WHERE FnStu_ID=@FnStu_ID";

            db.AddParameter("@FnStu_ID", System.Data.SqlDbType.Int, id);
      
            db.Exec();
            DataTable dt = db.Result;
            List<TSTU_B_Student> objs = new List<TSTU_B_Student>(); //objs相当于数据表ProductClass
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    //将当前行转换为具体的实体对象（obj相当行）
                    TSTU_B_Student obj = new TSTU_B_Student();
                    //obj实体的属性就等于当前行某列的值（属性相当于列）
                    obj.FsStu_Name = row["FsStu_Name"].ToString();
                    obj.FsStu_Account = row["FsStu_Account"].ToString();
                    obj.FsStu_Psw = row["FsStu_Psw"].ToString();
                    obj.FnStu_ID = int.Parse(row["FnStu_ID"].ToString());
                    obj.FnCla_ID = int.Parse(row["FnCla_ID"].ToString());
                    //将obj（行）添加到objs（表）
                    objs.Add(obj);
                }
            }
            //将当前行转换为具体的实体对象（obj相当行）
            result.ObjList = objs;
            return result;
        }
        /// <summary>
        /// 修改学生信息
        /// </summary>
        /// <param name="id">学生编号</param>
        /// <param name="FsStu_Account">账号</param>
        /// <param name="FsStu_Name">姓名</param>
        /// <param name="FsStu_Psw">密码</param>
        /// <param name="FnCla_ID">班级编号</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Student> StudentEdit(int? id,string FsStu_Account, string FsStu_Name, string FsStu_Psw, int FnCla_ID)
        {

            var result = new SvcResult<TSTU_B_Student>();
            if (string.IsNullOrWhiteSpace(FsStu_Account) || string.IsNullOrWhiteSpace(FsStu_Name) || string.IsNullOrWhiteSpace(FsStu_Psw))
            {
                result.Error = "数据错误";
            }
            if (FnCla_ID == 0)
            {
                result.Error = "数据错误(请补全信息)";
            }
            if (result.Error != null) return result;
            var db = new dbHelper(dbHelper.EnumServer.Local)
            {
                DataBase = "StudentDB",
                execType = dbHelper.ExecType.Exec,
                FuncError = ex =>
                {
                    //日志
                    result.Error = "系统内部错误";
                }
            };
            db.CmdText = "StudentDB.dbo.PR_PRC_StudentAdd";
            db.IsProc = true;
            db.AddParameter("@FnStu_ID", System.Data.SqlDbType.Int, id);
            db.AddParameter("@FnCla_ID", System.Data.SqlDbType.Int, FnCla_ID);
            db.AddParameter("@FsStu_Account", System.Data.SqlDbType.VarChar, FsStu_Account, 32);
            db.AddParameter("@FsStu_Psw", System.Data.SqlDbType.VarChar, FsStu_Psw, 32);
            db.AddParameter("@FsStu_Name", System.Data.SqlDbType.NVarChar, FsStu_Name, 32);
            db.NeedOutError = true;
            db.Exec();
            result.Error = db.OutParameters["Error"].ToString();
            if (db.Result == true)
            {
                result.ErrorCode = 1;//操作成功
                return result;
            }
            return result;
        }
        /// <summary>
        /// 总分详情
        /// </summary>
        /// <param name="id">学生编号</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Student> Sum_detil(int id)
        {
            var result = new SvcResult<TSTU_B_Student>();

            var db = new dbHelper(dbHelper.EnumServer.Local)
            {
                DataBase = "StudentDB",
                execType = dbHelper.ExecType.DataTable,
                FuncError = ex =>
                {
                    //日志
                    result.Error = "系统内部错误";
                }
            };
            db.CmdText = "SELECT * FROM StudentDB.dbo.TSTU_B_Student T1 INNER JOIN StudentDB.dbo.TSTU_B_Score T2 ON(T1.FnStu_ID=T2.FnStu_ID) INNER JOIN StudentDB.dbo.TSTU_B_Subject T3 ON(T2.FnSub_ID=T3.FnSub_ID) WHERE T1.FnStu_ID=@FnStu_ID ORDER BY T3.FsSub_Name";
            db.AddParameter("@FnStu_ID", System.Data.SqlDbType.Int, id);
            db.Exec();
            DataTable dt = db.Result;
            List<TSTU_B_Student> objs = new List<TSTU_B_Student>(); //objs相当于数据表ProductClass
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    //将当前行转换为具体的实体对象（obj相当行）
                    TSTU_B_Student obj = new TSTU_B_Student();
                    //obj实体的属性就等于当前行某列的值（属性相当于列）
                    obj.FsStu_Name = row["FsStu_Name"].ToString();
                    obj.FnStu_ID = int.Parse(row["FnStu_ID"].ToString());
                    obj.FsStu_Account = row["FsStu_Account"].ToString();
                    obj.FsSub_Name = row["FsSub_Name"].ToString();
                    if (!string.IsNullOrWhiteSpace(row["FnSco_Result"].ToString()))
                    {
                        obj.FnSco_Result = (decimal?)double.Parse(row["FnSco_Result"].ToString());
                    }
                    else
                    {
                        obj.FnSco_Result = null;
                    }
                    //将obj（行）添加到objs（表）
                    objs.Add(obj);
                }
            }
            //将当前行转换为具体的实体对象（obj相当行）
            result.ObjList = objs;
            return result;
        }
        /// <summary>
        /// 查询科目
        /// </summary>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Subject> Sel_Subject()
        {
            var result = new SvcResult<TSTU_B_Subject>();
            var db = new dbHelper(dbHelper.EnumServer.Local)
            {
                DataBase = "StudentDB",
                execType = dbHelper.ExecType.DataTable,
                FuncError = ex =>
                {
                    //日志
                    result.Error = "系统内部错误";
                }
            };
            db.CmdText = "SELECT FnSub_ID, FsSub_Name FROM StudentDB.dbo.TSTU_B_Subject ORDER BY FsSub_Name";
            db.Exec();
            DataTable dt = db.Result;
            List<TSTU_B_Subject> objs = new List<TSTU_B_Subject>(); //objs相当于数据表ProductClass
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    //将当前行转换为具体的实体对象（obj相当行）
                    TSTU_B_Subject obj = new TSTU_B_Subject();
                    //obj实体的属性就等于当前行某列的值（属性相当于列）
                    obj.FsSub_Name = row["FsSub_Name"].ToString();
                    obj.FnSub_ID = int.Parse(row["FnSub_ID"].ToString());
                    //将obj（行）添加到objs（表）
                    objs.Add(obj);
                }
            }
            //将当前行转换为具体的实体对象（obj相当行）
            result.ObjList = objs;
            return result;
        }
    }
}
