﻿using System;
using System.Collections.Generic;
using System.Data;
using StudentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentDAL
{
    public class LoginDal
    {
        /// <summary>
        /// 学生登录
        /// </summary>
        /// <param name="account">账号</param>
        /// <param name="pwd">密码</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Student> Login_stu(string account,string pwd)
        {
            var result = new SvcResult<TSTU_B_Student>();
            string _Name = null, _Pwd = null; int _FnCla = 0, _ID = 0;
            var stu = new TSTU_B_Student();
            var db = new dbHelper(dbHelper.EnumServer.Local)
            {
                DataBase = "StudentDB",
                execType = dbHelper.ExecType.IfRead,
                FuncError = ex =>
                {
                    //日志
                    result.Error = "系统内部错误";
                    //result.Error = ex.ToString();
                }
            };
            db.CmdText = "SELECT FnStu_ID,FnCla_ID,FsStu_Account,FsStu_Psw,FsStu_Name FROM StudentDB.dbo.TSTU_B_Student where FsStu_Account=@FsStu_Account";
            db.AddParameter("@FsStu_Account", System.Data.SqlDbType.VarChar, account, 32);
            db.FuncRead = sdr =>
            {
                _Name = sdr["FsStu_Name"].ToString();
                _Pwd = sdr["FsStu_Psw"].ToString();
                _ID = int.Parse(sdr["FnStu_ID"].ToString());
                _FnCla = int.Parse(sdr["FnCla_ID"].ToString());
            };
            db.Exec();
            if (result.Error != null) return result;
            if (!db.Result)
            {
                result.Error = "用户不存在";
                return result;
            }
            if (pwd != _Pwd)
            {
                result.Error = "密码错误";
                return result;
            }
            stu.FnCla_ID = _FnCla;
            stu.FnStu_ID = _ID;
            stu.FsStu_Name = _Name;
            stu.FsStu_Account = account;
            result.Obj = stu;
            return result;
        }
        /// <summary>
        /// 查询班级（根据id查）
        /// </summary>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Class> Sel_ClassId(int id)
        {
            var result = new SvcResult<TSTU_B_Class>();

            var db = new dbHelper(dbHelper.EnumServer.Local)
            {
                DataBase = "StudentDB",
                execType = dbHelper.ExecType.IfRead,
                FuncError = ex =>
                {
                    //日志
                    result.Error = "系统内部错误";
                }
            };
            db.CmdText = "SELECT FnCla_ID, FsCla_Name FROM StudentDB.dbo.TSTU_B_Class WHERE FnCla_ID=@FnCla_ID";
            db.AddParameter("@FnCla_ID", System.Data.SqlDbType.Int, id);
            TSTU_B_Class obj = new TSTU_B_Class();
            db.FuncRead = sdr =>
            {
                obj.FnCla_ID = Convert.ToInt32(sdr["FnCla_ID"].ToString());
                obj.FsCla_Name = sdr["FsCla_Name"].ToString();
            };
            db.Exec();
            //将当前行转换为具体的实体对象（obj相当行）
            result.Obj = obj;
            return result;
        }
        /// <summary>
        /// 查询班级总人数
        /// </summary>
        /// <param name="id">班级id</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Class> Sel_ClassSum(int id)
        {
            var result = new SvcResult<TSTU_B_Class>();

            var db = new dbHelper(dbHelper.EnumServer.Local)
            {
                DataBase = "StudentDB",
                execType = dbHelper.ExecType.IfRead,
                FuncError = ex =>
                {
                    //日志
                    result.Error = "系统内部错误";
                }
            };
            db.CmdText = "SELECT COUNT(*) count FROM StudentDB.dbo.TSTU_B_Class T1 INNER JOIN StudentDB.dbo.TSTU_B_Student T2 ON(T1.FnCla_ID=T2.FnCla_ID) WHERE T1.FnCla_ID=@FnCla_ID";
            db.AddParameter("@FnCla_ID", System.Data.SqlDbType.Int, id);
            TSTU_B_Class obj = new TSTU_B_Class();
            db.FuncRead = sdr =>
            {
                obj.count = int.Parse(sdr["count"].ToString());
            };
            db.Exec();
            //将当前行转换为具体的实体对象（obj相当行）
            result.Obj = obj;
            return result;
        }
        /// <summary>
        /// 查询班级科目数
        /// </summary>
        /// <param name="id">科目id</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Class> Sel_ClassSubjectSum(int id)
        {
            var result = new SvcResult<TSTU_B_Class>();

            var db = new dbHelper(dbHelper.EnumServer.Local)
            {
                DataBase = "StudentDB",
                execType = dbHelper.ExecType.IfRead,
                FuncError = ex =>
                {
                    //日志
                    result.Error = "系统内部错误";
                }
            };
            db.CmdText = "SELECT COUNT(*) count FROM StudentDB.dbo.TSTU_B_Class T1 INNER JOIN StudentDB.dbo.TSTU_B_Teacher T2 ON(T1.FnCla_ID=T2.FnCla_ID) INNER JOIN StudentDB.dbo.TSTU_B_Subject T3 ON(T2.FnSub_ID=T3.FnSub_ID) WHERE T1.FnCla_ID=@FnCla_ID";
            db.AddParameter("@FnCla_ID", System.Data.SqlDbType.Int, id);
            TSTU_B_Class obj = new TSTU_B_Class();
            db.FuncRead = sdr =>
            {
                obj.count = int.Parse(sdr["count"].ToString());
            };
            db.Exec();
            //将当前行转换为具体的实体对象（obj相当行）
            result.Obj = obj;
            return result;
        }
        /// <summary>
        /// 查询个人成绩
        /// </summary>
        /// <param name="id">学生编号</param>
        /// <param name="FnCla_ID">班级编号</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Student> Personal(int id,int FnCla_ID)
        {
            var result = new SvcResult<TSTU_B_Student>();

            var db = new dbHelper(dbHelper.EnumServer.Local)
            {
                DataBase = "StudentDB",
                execType = dbHelper.ExecType.DataTable,
                FuncError = ex =>
                {
                    //日志
                    result.Error = "系统内部错误";
                }
            };
            db.CmdText = "SELECT row_number() over(ORDER BY T2.FnStu_ID) AS rownumber,* FROM StudentDB.dbo.TSTU_B_Student T1 INNER JOIN TSTU_B_Score T2 ON (T1.FnStu_ID=T2.FnStu_ID) INNER JOIN TSTU_B_Subject T3 ON (T2.FnSub_ID=T3.FnSub_ID)  INNER JOIN TSTU_B_Teacher T4 ON (T4.FnSub_ID=T3.FnSub_ID) INNER JOIN TSTU_B_Class T5 ON (T4.FnCla_ID=T5.FnCla_ID) WHERE T2.FnStu_ID=@FnStu_ID AND T4.FnCla_ID=@FnCla_ID";
            db.AddParameter("@FnStu_ID", System.Data.SqlDbType.Int, id);
            db.AddParameter("@FnCla_ID", System.Data.SqlDbType.Int, FnCla_ID);
            db.Exec();
            DataTable dt = db.Result;
            List<TSTU_B_Student> objs = new List<TSTU_B_Student>(); //objs相当于数据表ProductClass
            if (dt!=null&&dt.Rows.Count>0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    //将当前行转换为具体的实体对象（obj相当行）
                    TSTU_B_Student obj = new TSTU_B_Student();
                    //obj实体的属性就等于当前行某列的值（属性相当于列）
                    obj.FsStu_Name = row["FsStu_Name"].ToString();
                    obj.FnStu_ID= int.Parse(row["FnStu_ID"].ToString());
                    obj.FsStu_Account = row["FsStu_Account"].ToString();
                    obj.FsCla_Name = row["FsCla_Name"].ToString();
                    obj.FsSub_Name = row["FsSub_Name"].ToString();
                    obj.FsTea_Name = row["FsTea_Name"].ToString();
                    obj.rownumber= int.Parse(row["rownumber"].ToString());
                    if (!string.IsNullOrWhiteSpace(row["FnSco_Result"].ToString()))
                    {
                        obj.FnSco_Result = (decimal?)double.Parse(row["FnSco_Result"].ToString());
                    }
                    else
                    {
                        obj.FnSco_Result = null;
                    }
                    //将obj（行）添加到objs（表）
                    objs.Add(obj);
                }
            }
            //将当前行转换为具体的实体对象（obj相当行）
            result.ObjList = objs;
            return result;
        }
        /// <summary>
        /// 各科成绩
        /// </summary>
        /// <param name="id">科目编号</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Student> Subject_Result(int? id)
        {
            var result = new SvcResult<TSTU_B_Student>();

            var db = new dbHelper(dbHelper.EnumServer.Local)
            {
                DataBase = "StudentDB",
                execType = dbHelper.ExecType.DataTable,
                FuncError = ex =>
                {
                    //日志
                    result.Error = "系统内部错误";
                }
            };
            if (id==null)
            {
                db.CmdText = "SELECT COUNT(*) over() AS cnt,row_number() over(ORDER BY T2.FnSco_Result DESC) AS rownumber,T3.FsStu_Account,T3.FsStu_Name,T4.FsCla_Name,T1.FsSub_Name,T2.FnSco_Result FROM StudentDB.dbo.TSTU_B_Subject T1 INNER JOIN StudentDB.dbo.TSTU_B_Score T2 ON(T2.FnSub_ID = T1.FnSub_ID) INNER JOIN StudentDB.dbo.TSTU_B_Student T3 ON(T2.FnStu_ID = T3.FnStu_ID) INNER JOIN StudentDB.dbo.TSTU_B_Class T4 ON(T4.FnCla_ID = T3.FnCla_ID)";
            }
            else
            {
                db.CmdText = "SELECT COUNT(*) over() AS cnt,row_number() over(ORDER BY T2.FnSco_Result DESC) AS rownumber,T3.FsStu_Account,T3.FsStu_Name,T4.FsCla_Name,T1.FsSub_Name,T2.FnSco_Result FROM StudentDB.dbo.TSTU_B_Subject T1 INNER JOIN StudentDB.dbo.TSTU_B_Score T2 ON(T2.FnSub_ID = T1.FnSub_ID) INNER JOIN StudentDB.dbo.TSTU_B_Student T3 ON(T2.FnStu_ID = T3.FnStu_ID) INNER JOIN StudentDB.dbo.TSTU_B_Class T4 ON(T4.FnCla_ID = T3.FnCla_ID) WHERE T2.FnSub_ID = @FnSub_ID";
                db.AddParameter("@FnSub_ID", System.Data.SqlDbType.Int, id);
            }
            db.Exec();
            DataTable dt = db.Result;
            List<TSTU_B_Student> objs = new List<TSTU_B_Student>(); //objs相当于数据表ProductClass
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    //将当前行转换为具体的实体对象（obj相当行）
                    TSTU_B_Student obj = new TSTU_B_Student();
                    //obj实体的属性就等于当前行某列的值（属性相当于列）
                    obj.FsStu_Name = row["FsStu_Name"].ToString();
                    obj.FsStu_Account = row["FsStu_Account"].ToString();
                    obj.FsCla_Name = row["FsCla_Name"].ToString();
                    obj.FsSub_Name = row["FsSub_Name"].ToString();
                    obj.rownumber = int.Parse(row["rownumber"].ToString());
                    if (!string.IsNullOrWhiteSpace(row["FnSco_Result"].ToString()))
                    {
                        obj.FnSco_Result = (decimal?)double.Parse(row["FnSco_Result"].ToString());
                    }
                    else
                    {
                        obj.FnSco_Result = null;
                    }
                    //将obj（行）添加到objs（表）
                    objs.Add(obj);
                }
            }
            //将当前行转换为具体的实体对象（obj相当行）
            result.ObjList = objs;
            return result;
        }
        /// <summary>
        /// 总分查询
        /// </summary>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Score> Result(string name)
        {
            var result = new SvcResult<TSTU_B_Score>();

            var db = new dbHelper(dbHelper.EnumServer.Local)
            {
                DataBase = "StudentDB",
                execType = dbHelper.ExecType.DataTable,
                FuncError = ex =>
                {
                    //日志
                    result.Error = "系统内部错误";
                }
            };
            if (string.IsNullOrWhiteSpace(name))
            {
                db.CmdText = "SELECT COUNT(*) over() AS cnt,row_number() over(order by SUM(T2.FnSco_Result) DESC) AS rownumber,T2.FnStu_ID,T1.FsStu_Account, T1.FsStu_Name,T4.FsCla_Name,t4.FnCla_ID,SUM(T2.FnSco_Result) sum,convert(decimal(10,2),AVG(T2.FnSco_Result)) avg FROM StudentDB.dbo.TSTU_B_Student T1 INNER JOIN StudentDB.dbo.TSTU_B_Score T2 ON(T1.FnStu_ID = T2.FnStu_ID) INNER JOIN StudentDB.dbo.TSTU_B_Subject T3 ON(T2.FnSub_ID = T3.FnSub_ID) INNER JOIN StudentDB.dbo.TSTU_B_Class T4 ON(T4.FnCla_ID = T1.FnCla_ID) GROUP BY T2.FnStu_ID,T4.FnCla_ID,T1.FsStu_Name,T4.FsCla_Name,T1.FsStu_Account";
            }
            else
            {
                db.CmdText = "SELECT COUNT(*) over() AS cnt,row_number() over(order by SUM(T2.FnSco_Result) DESC) AS rownumber,T2.FnStu_ID,T1.FsStu_Account, T1.FsStu_Name,T4.FsCla_Name,t4.FnCla_ID,SUM(T2.FnSco_Result) sum,convert(decimal(10,2),AVG(T2.FnSco_Result)) avg FROM StudentDB.dbo.TSTU_B_Student T1 INNER JOIN StudentDB.dbo.TSTU_B_Score T2 ON(T1.FnStu_ID = T2.FnStu_ID) INNER JOIN StudentDB.dbo.TSTU_B_Subject T3 ON(T2.FnSub_ID = T3.FnSub_ID) INNER JOIN StudentDB.dbo.TSTU_B_Class T4 ON(T4.FnCla_ID = T1.FnCla_ID) GROUP BY T2.FnStu_ID,T4.FnCla_ID,T1.FsStu_Name,T4.FsCla_Name,T1.FsStu_Account HAVING T1.FsStu_Name LIKE '%'+@FsStu_Name+'%'";
                db.AddParameter("@FsStu_Name", System.Data.SqlDbType.NVarChar, name,32);
            }
            
            db.Exec();
            DataTable dt = db.Result;
            List<TSTU_B_Score> objs = new List<TSTU_B_Score>(); //objs相当于数据表ProductClass
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    //将当前行转换为具体的实体对象（obj相当行）
                    TSTU_B_Score obj = new TSTU_B_Score();
                    //obj实体的属性就等于当前行某列的值（属性相当于列）
                    obj.FsStu_Name = row["FsStu_Name"].ToString();
                    obj.FsStu_Name = row["FsStu_Name"].ToString();
                    obj.FsStu_Account = row["FsStu_Account"].ToString();
                    obj.FsCla_Name = row["FsCla_Name"].ToString();
                    obj.FnCla_ID = int.Parse(row["FnCla_ID"].ToString());
                    obj.FnStu_ID = int.Parse(row["FnStu_ID"].ToString());
                    obj.rownumber = int.Parse(row["rownumber"].ToString());
                    if (!string.IsNullOrWhiteSpace(row["sum"].ToString()))
                    {
                        obj.sum = (decimal?)double.Parse(row["sum"].ToString());
                    }
                    else
                    {
                        obj.sum = null;
                    }
                    if (!string.IsNullOrWhiteSpace(row["avg"].ToString()))
                    {
                        obj.avg = (decimal?)double.Parse(row["avg"].ToString());
                    }
                    else
                    {
                        obj.avg = null;
                    }
                    //将obj（行）添加到objs（表）
                    objs.Add(obj);
                }
            }
            //将当前行转换为具体的实体对象（obj相当行）
            result.ObjList = objs;
            return result;
        }
        /// <summary>
        /// 重置密码
        /// </summary>
        /// <param name="Account">账号</param>
        /// <param name="type">账号类型（1：学生，2：教师，3：管理员）</param>
        /// <returns></returns>
        public static SvcResult<TSTU_B_Student> ForgotPassword(string Account,int? type)
        {

            var result = new SvcResult<TSTU_B_Student>();
            if (string.IsNullOrWhiteSpace(Account))
            {
                result.Error = "数据错误";
            }
            if (type == 0||type==null)
            {
                result.Error = "数据错误(请补全信息)";
            }
            if (result.Error != null) return result;
            var db = new dbHelper(dbHelper.EnumServer.Local)
            {
                DataBase = "StudentDB",
                execType = dbHelper.ExecType.Exec,
                FuncError = ex =>
                {
                    //日志
                    result.Error = "系统内部错误";
                }
            };
            db.CmdText = "PR_PRC_ResetPassword";
            db.IsProc = true;
            db.AddParameter("@Type", System.Data.SqlDbType.Int, type);
            db.AddParameter("@Account", System.Data.SqlDbType.VarChar, Account, 32);
            db.NeedOutError = true;
            db.Exec();
            result.Error = db.OutParameters["Error"].ToString();
            return result;
        }
    }
}
