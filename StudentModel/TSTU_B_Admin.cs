namespace StudentModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TSTU_B_Admin
    {
        [Key]
        public int FnAdm_ID { get; set; }

        [Required]
        [StringLength(32)]
        public string FsAdm_Name { get; set; }

        [Required]
        [StringLength(32)]
        public string FsAdm_Account { get; set; }

        [Required]
        [StringLength(32)]
        public string FsAdm_Psw { get; set; }
    }
}
