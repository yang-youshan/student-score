﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Data;

namespace StudentModel
{
    /// <summary>
    /// 接口统一返回对象
    /// <para>调用接口返回结果后，获取Error属性判断接口是否操作成功</para>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [DataContract(Name = "SvcResult_{0}{#}")]
    public class SvcResult<T>
    {
        int _ErrorCode = 0;
        /// <summary>
        /// 错误代码
        /// </summary>
        [DataMember]
        public int ErrorCode
        {
            get { return _ErrorCode; }
            set { _ErrorCode = value; }
        }

        private string _Error = null;
        /// <summary>
        /// 错误信息
        /// </summary>
        [DataMember]
        public string Error
        {
            get { return _Error; }
            set { _Error = value; }
        }

        private T _Obj;
        /// <summary>
        /// 单个对象
        /// </summary>
        [DataMember]
        public T Obj
        {
            get { return _Obj; }
            set { _Obj = value; }
        }

        private List<T> _ObjList = new List<T>();
        /// <summary>
        /// 列表对象
        /// </summary>
        [DataMember]
        public List<T> ObjList
        {
            get { return _ObjList; }
            set { _ObjList = value; }
        }
        //private DataTable _ObjDataTable = new DataTable();
        ///// <summary>
        ///// DataTable对象
        ///// </summary>
        //[DataMember]
        //public DataTable ObjDataTable
        //{
        //    get { return _ObjDataTable; }
        //    set { _ObjDataTable = value; }
        //}

        private DataSet _ObjDataSet = new DataSet();
        /// <summary>
        /// DataSet对象
        /// </summary>
        [DataMember]
        public DataSet ObjDataSet
        {
            get { return _ObjDataSet; }
            set { _ObjDataSet = value; }
        }
    }
}
