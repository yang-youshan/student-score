﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentModel
{
    public class MyModel
    {
        public List<TSTU_B_Student> T1 { get; set; }
        public List<TSTU_B_Score> T2 { get; set; }
        public MyModel()
        {
            StudentDB db = new StudentDB();
            T1 = db.TSTU_B_Student.ToList();
            T2 = db.TSTU_B_Score.ToList();

        }
    }
}
