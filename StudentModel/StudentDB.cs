using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace StudentModel
{
    public partial class StudentDB : DbContext
    {
        public StudentDB()
            : base("name=StudentDB")
        {
        }

        public virtual DbSet<TSTU_B_Admin> TSTU_B_Admin { get; set; }
        public virtual DbSet<TSTU_B_Class> TSTU_B_Class { get; set; }
        public virtual DbSet<TSTU_B_Score> TSTU_B_Score { get; set; }
        public virtual DbSet<TSTU_B_Student> TSTU_B_Student { get; set; }
        public virtual DbSet<TSTU_B_Subject> TSTU_B_Subject { get; set; }
        public virtual DbSet<TSTU_B_Teacher> TSTU_B_Teacher { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TSTU_B_Admin>()
                .Property(e => e.FsAdm_Account)
                .IsUnicode(false);

            modelBuilder.Entity<TSTU_B_Admin>()
                .Property(e => e.FsAdm_Psw)
                .IsUnicode(false);

            modelBuilder.Entity<TSTU_B_Score>()
                .Property(e => e.FnSco_Result)
                .HasPrecision(10, 2);

            modelBuilder.Entity<TSTU_B_Student>()
                .Property(e => e.FsStu_Account)
                .IsUnicode(false);

            modelBuilder.Entity<TSTU_B_Student>()
                .Property(e => e.FsStu_Psw)
                .IsUnicode(false);

            modelBuilder.Entity<TSTU_B_Teacher>()
                .Property(e => e.FsTea_Psw)
                .IsUnicode(false);

            modelBuilder.Entity<TSTU_B_Teacher>()
                .Property(e => e.FsTea_Account)
                .IsUnicode(false);
        }
    }
}
