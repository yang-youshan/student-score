namespace StudentModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TSTU_B_Teacher
    {
        [Key]
        [Display(Name = "教师编号")]
        public int FnTea_ID { get; set; }
        [Display(Name = "科目编号")]
        public int FnSub_ID { get; set; }
        [Display(Name = "教师班级")]
        public int FnCla_ID { get; set; }

        [Required]
        [StringLength(32)]
        [Display(Name = "教师姓名")]
        public string FsTea_Name { get; set; }

        [Required]
        [StringLength(32)]
        [Display(Name = "教师密码")]
        public string FsTea_Psw { get; set; }

        [StringLength(32)]
        [Display(Name = "教师账号")]
        public string FsTea_Account { get; set; }
        //外
        public int count { get; set; }
        public int rownumber { get; set; }
        public string FsCla_Name { get; set; }
        public string FsSub_Name { get; set; }
    }
}
