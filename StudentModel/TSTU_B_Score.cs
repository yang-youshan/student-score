namespace StudentModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TSTU_B_Score
    {
        [Key]
        public int FnSco_ID { get; set; }

        public int FnStu_ID { get; set; }

        public int FnSub_ID { get; set; }

        public decimal? FnSco_Result { get; set; }
        //���
        
        public int rownumber { get; set; }
        public string FsCla_Name { get; set; }
        public string FsStu_Name { get; set; }
        public string FsStu_Account { get; set; }
        public string FsSub_Name { get; set; }
        public decimal? sum { get; set; }
        public decimal? avg { get; set; }
        public int FnCla_ID { get; set; }
    }
}
