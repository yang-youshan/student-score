namespace StudentModel
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TSTU_B_Student
    {
        [Key]
        [Display(Name = "学生编号")]
        public int FnStu_ID { get; set; }
        [Display(Name = "学生班级")]
        public int FnCla_ID { get; set; }

        [Required]
        [StringLength(32)]
        [Display(Name = "学生账号")]
        public string FsStu_Account { get; set; }

        [Required]
        [StringLength(32)]
        [Display(Name = "学生密码")]
        public string FsStu_Psw { get; set; }
        string _FsStu_Name;
        [Required]
        [StringLength(32)]
        [Display(Name ="学生姓名")]
        public string FsStu_Name
        {
            get { return _FsStu_Name; }
            set { _FsStu_Name = value; OnPropertyChanged("FsStu_Name"); }
        }
        //外表
        public int rownumber { get; set; }
        public int cnt { get; set; }
        public string FsCla_Name { get; set; }
        public string FsSub_Name { get; set; }
        public decimal? sum { get; set; }
        public decimal? avg { get; set; }
        public string FsTea_Name { get; set; }
        public decimal? FnSco_Result { get; set; }
        public IEnumerable list { get; set; }
        protected internal virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
