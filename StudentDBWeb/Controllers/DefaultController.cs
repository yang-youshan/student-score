﻿using StudentModel;
using System;
using StudentBLL;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace StudentDBWeb.Controllers
{
    public class DefaultController : Controller
    {
        int _currentPage = 1;
        int _pageSize = 7;
        private IList<TSTU_B_Student> _allProducts = new List<TSTU_B_Student>();
        StudentDB db = new StudentDB();
        // GET:
        // 
        public ActionResult Index()
        {
            if (Session["Name"] != null)
            {
                ViewBag.Name = Session["Name"];
            }
            if (Session["FsCla_Name"] != null)
            {
                ViewBag.FsCla_Name = Session["FsCla_Name"];
            }
            if (Session["count"] != null)
            {
                ViewBag.count = Session["count"];
            }
            if (Session["Subject_count"] != null)
            {
                ViewBag.Subject_count = Session["Subject_count"];
            }
            if (Session["id"] != null)
            {
                ViewBag.id = Session["id"];
            }
            if (Session["Name"] == null || Session["FsCla_Name"] == null || Session["count"] == null || Session["Subject_count"] == null)
            {
                return Redirect("/Default/Null404");
            }
            return View();
        }
        /// <summary>
        /// 个人成绩
        /// </summary>
        /// <returns></returns>
        public int stuid = 0;
        public ActionResult Personal(int? id, int? size, int? page = 1)
        {
            if (Session["Name"] != null)
            {
                ViewBag.Name = Session["Name"];
            }
            if (Session["FsCla_Name"] != null)
            {
                ViewBag.FsCla_Name = Session["FsCla_Name"];
            }
            if (Session["count"] != null)
            {
                ViewBag.count = Session["count"];
            }
            if (Session["Subject_count"] != null)
            {
                ViewBag.Subject_count = Session["Subject_count"];
            }
            if (Session["id"] != null)
            {
                ViewBag.id = Session["id"];
                if (id == null)
                {
                    id = int.Parse(Session["id"].ToString());
                }
            }
            if (id == 0 || id == null)
            {
                return Redirect("/Default/Null404");
            }
            if (Session["Name"] != null)
            {
                ViewBag.Name = Session["Name"];
            }
            SvcResult<TSTU_B_Student> Personal = LoginBll.Personal((int)id, int.Parse(Session["classid"].ToString()));
            if (Personal == null)
            {
                return Redirect("/Default/Null404");
            }
            _currentPage = page.HasValue ? page.Value : _currentPage;
            _pageSize = size.HasValue ? size.Value : _pageSize;
            _allProducts = Personal.ObjList;
            ViewBag.size = _pageSize;
            var model = _allProducts.ToPagedList(_currentPage, _pageSize);
            ViewBag.PageCount = Math.Ceiling((double)model.TotalItemCount / (double)model.PageSize);
            return View(model);
        }
        /// <summary>
        /// 总分
        /// </summary>
        /// <returns></returns>
        public ActionResult Total(string name, int? size, int? page = 1)
        {
            ViewBag.stuname = name;
            if (Session["Name"] != null)
            {
                ViewBag.Name = Session["Name"];
            }
            if (Session["FsCla_Name"] != null)
            {
                ViewBag.FsCla_Name = Session["FsCla_Name"];
            }
            if (Session["count"] != null)
            {
                ViewBag.count = Session["count"];
            }
            if (Session["Subject_count"] != null)
            {
                ViewBag.Subject_count = Session["Subject_count"];
            }
            if (Session["id"] != null)
            {
                ViewBag.id = Session["id"];
            }
            if (Session["Name"] == null || Session["FsCla_Name"] == null || Session["count"] == null || Session["Subject_count"] == null)
            {
                return Redirect("/Default/Null404");
            }
            SvcResult<TSTU_B_Score> Result = LoginBll.Result(name);
            ViewBag.total = Result.ObjList;
            if (Result == null)
            {
                return Redirect("/Default/Null404");
            }
            _currentPage = page.HasValue ? page.Value : _currentPage;
            _pageSize = size.HasValue ? size.Value : _pageSize;
            @ViewBag.size = _pageSize;
            var model = Result.ObjList.ToPagedList(_currentPage, _pageSize);
            ViewBag.PageCount = Math.Ceiling((double)model.TotalItemCount / (double)model.PageSize);
            return View(model);
        }


        [HttpPost]
        public ActionResult Total(int? id)
        {
            if (id == 0)
            {
                return Redirect("/Default/Null404");
            }
            SvcResult<TSTU_B_Student> Personal = TeacherBll.Sum_detil((int)id);
            if (Personal.ObjList.Count > 0)
            {
                ViewBag.Personal = Personal.ObjList;
            }
            if (Personal == null)
            {
                return Redirect("/Default/Null404");
            }

            return Json(Personal.ObjList);
        }
        /// <summary>
        /// 各科排名
        /// </summary>
        /// <returns></returns>
        public ActionResult Subject(int? id, int? size, int? page = 1)
        {
            ViewBag.subid = id;
            if (Session["Name"] != null)
            {
                ViewBag.Name = Session["Name"];
            }
            if (Session["FsCla_Name"] != null)
            {
                ViewBag.FsCla_Name = Session["FsCla_Name"];
            }
            if (Session["count"] != null)
            {
                ViewBag.count = Session["count"];
            }
            if (Session["Subject_count"] != null)
            {
                ViewBag.Subject_count = Session["Subject_count"];
            }
            if (Session["id"] != null)
            {
                ViewBag.id = Session["id"];
            }
            if (Session["Name"] == null || Session["FsCla_Name"] == null || Session["count"] == null || Session["Subject_count"] == null)
            {
                return Redirect("/Default/Null404");
            }
            SvcResult<TSTU_B_Subject> Sub = TeacherBll.Sel_Subject();
            ViewBag.sub = Sub.ObjList;
            SvcResult<TSTU_B_Student> Subject_Result = LoginBll.Subject_Result(id);
            if (Subject_Result == null)
            {
                return Redirect("/Default/Null404");
            }
            //分页
            _currentPage = page.HasValue ? page.Value : _currentPage;
            _pageSize = size.HasValue ? size.Value : _pageSize;
            ViewBag.size = _pageSize;
            var model = Subject_Result.ObjList.ToPagedList(_currentPage, _pageSize);
            ViewBag.PageCount = Math.Ceiling((double)model.TotalItemCount / (double)model.PageSize);
            return View(model);
        }
        [HttpPost]
        public ActionResult Subject(int id)
        {
            SvcResult<TSTU_B_Subject> Sub = TeacherBll.Sel_Subject();
            ViewBag.sub = Sub.ObjList;
            SvcResult<TSTU_B_Student> Subject_Result = LoginBll.Subject_Result(id);
            ViewBag.sublist = Subject_Result.ObjList;
            if (Subject_Result == null)
            {
                return Redirect("/Default/Null404");
            }
            ViewBag.Subject = Subject_Result.ObjList;
            //分页
            _allProducts = Subject_Result.ObjList;
            var model = _allProducts.ToPagedList(_currentPage, _pageSize);
            return Json(Subject_Result.ObjList);
        }
        /// <summary>
        /// 登录
        /// </summary>
        /// <returns></returns>
        public ActionResult Login()
        {
            //获取cookie中的数据
            HttpCookie cookie = Request.Cookies.Get("Example");
            //判断cookie是否空值
            if (cookie != null)
            {
                //把保存的用户名和密码赋值给对应的文本框
                //用户名

                var name = cookie.Values["UserName"].ToString();
                ViewBag.UserName = name;
                //密码
                var pwd = cookie.Values["UserPwd"].ToString();
                ViewBag.UserPwd = pwd;
                //账号类型
                if (cookie.Values.AllKeys.Contains("UserType"))
                {
                    var type = cookie.Values["UserType"].ToString();
                    ViewBag.UserType = type;
                }
                
            }
            //ViewBag.UserName = Retrieve("UserName");
            Session.Clear();
            return View();
        }
        [HttpPost]
        public ActionResult Login(string account, string psw, string type, bool Ck)
        {
            ResultErr err = new ResultErr();
            if (Request.IsAjaxRequest())
            {
                if (string.IsNullOrWhiteSpace(account))
                {
                    err.Msg = "错误信息：账号为空";
                    err.Status = 0;
                    return Json(err);
                }
                if (string.IsNullOrWhiteSpace(psw))
                {
                    err.Msg = "错误信息：密码为空";
                    err.Status = 0;
                    return Json(err);
                }
                if (type == "1")
                {
                    SvcResult<TSTU_B_Student> ret = LoginBll.Login_stu(account, psw);
                    if (ret.Error != null)
                    {
                        err.Msg = "错误信息：" + ret.Error;
                        err.Status = 0;
                        return Json(err);
                    }
                    //判断是否记住密码
                    if (Ck)
                    {
                        HttpCookie hc = new HttpCookie("Example");

                        //在cookie对象中保存用户名和密码
                        hc["UserName"] = account;
                        hc["UserPwd"] = psw;
                        hc["UserType"] = type;
                        //设置过期时间
                        hc.Expires = DateTime.Now.AddDays(2);
                        //保存到客户端
                        Response.Cookies.Add(hc);
                    }
                    else
                    {
                        HttpCookie hc = new HttpCookie("Example");
                        //判断hc是否空值
                        if (hc != null)
                        {
                            //设置过期时间
                            hc.Expires = DateTime.Now.AddDays(-1);
                            //保存到客户端
                            Response.Cookies.Add(hc);
                        }

                    }
                    err.Msg = "登录成功";
                    err.Status = 1;
                    Session["Name"] = ret.Obj.FsStu_Name;
                    Session["classid"] = ret.Obj.FnCla_ID;
                    //classid = ret.Obj.FnCla_ID;
                    SvcResult<TSTU_B_Class> B_Class = LoginBll.Sel_ClassId(ret.Obj.FnCla_ID);
                    Session["FsCla_Name"] = B_Class.Obj.FsCla_Name;
                    SvcResult<TSTU_B_Class> ClassSum = LoginBll.Sel_ClassSum(ret.Obj.FnCla_ID);
                    Session["count"] = ClassSum.Obj.count;
                    SvcResult<TSTU_B_Class> ClassSubjectSum = LoginBll.Sel_ClassSubjectSum(ret.Obj.FnCla_ID);
                    Session["Subject_count"] = ClassSubjectSum.Obj.count;
                    Session["id"] = ret.Obj.FnStu_ID;
                    stuid = ret.Obj.FnStu_ID;
                    Redirect("/Default/Index");
                    return Json(err);
                }
                else if (type == "2")
                {
                    SvcResult<TSTU_B_Teacher> ret = TeacherBll.Login(account, psw);
                    if (ret.Error != null)
                    {
                        err.Msg = "错误信息：" + ret.Error;
                        err.Status = 0;
                        return Json(err);
                    }
                    err.Msg = "登录成功";
                    err.Status = 1;
                    //判断是否记住密码
                    if (Ck)
                    {
                        HttpCookie hc = new HttpCookie("Example");

                        //在cookie对象中保存用户名和密码
                        hc["UserName"] = account;
                        hc["UserPwd"] = psw;
                        hc["UserType"] = type;
                        //设置过期时间
                        hc.Expires = DateTime.Now.AddDays(2);
                        //保存到客户端
                        Response.Cookies.Add(hc);
                    }
                    else
                    {
                        HttpCookie hc = new HttpCookie("Example");
                        //判断hc是否空值
                        if (hc != null)
                        {
                            //设置过期时间
                            hc.Expires = DateTime.Now.AddDays(-1);
                            //保存到客户端
                            Response.Cookies.Add(hc);
                        }

                    }
                    Session["Name"] = ret.Obj.FsTea_Name;
                    //classid = ret.Obj.FnCla_ID;
                    SvcResult<TSTU_B_Class> B_Class = LoginBll.Sel_ClassId(ret.Obj.FnCla_ID);
                    Session["FnCla_ID"] = ret.Obj.FnCla_ID;
                    Session["FnSub_ID"] = ret.Obj.FnSub_ID;
                    Session["FsCla_Name"] = B_Class.Obj.FsCla_Name;
                    SvcResult<TSTU_B_Class> ClassSum = LoginBll.Sel_ClassSum(ret.Obj.FnCla_ID);
                    Session["count"] = ClassSum.Obj.count;
                    SvcResult<TSTU_B_Class> ClassSubjectSum = LoginBll.Sel_ClassSubjectSum(ret.Obj.FnCla_ID);
                    Session["Subject_count"] = ClassSubjectSum.Obj.count;
                    Session["id"] = ret.Obj.FnTea_ID;
                    stuid = ret.Obj.FnTea_ID;
                    return Json(err);
                }
                else if (type == "3")
                {
                    SvcResult<TSTU_B_Admin> ret = AdminBll.Login(account, psw);
                    if (ret.Error != null)
                    {
                        err.Msg = "错误信息：" + ret.Error;
                        err.Status = 0;
                        return Json(err);
                    }
                    err.Msg = "登录成功";
                    err.Status = 1;
                    //判断是否记住密码
                    if (Ck)
                    {
                        HttpCookie hc = new HttpCookie("Example");

                        //在cookie对象中保存用户名和密码
                        hc["UserName"] = account;
                        hc["UserPwd"] = psw;
                        hc["UserType"] = type;
                        //设置过期时间
                        hc.Expires = DateTime.Now.AddDays(2);
                        //保存到客户端
                        Response.Cookies.Add(hc);
                    }
                    else
                    {
                        HttpCookie hc = new HttpCookie("Example");
                        //判断hc是否空值
                        if (hc != null)
                        {
                            //设置过期时间
                            hc.Expires = DateTime.Now.AddDays(-1);
                            //保存到客户端
                            Response.Cookies.Add(hc);
                        }

                    }
                    Session["Name"] = ret.Obj.FsAdm_Name;
                    //classid = ret.Obj.FnCla_ID;
                    Session["FsCla_Name"] = "管理员";
                    //SvcResult<TSTU_B_Class> B_Class = LoginBll.Sel_ClassId(ret.Obj.FnCla_ID);
                    //Session["FnCla_ID"] = ret.Obj.FnCla_ID;
                    //Session["FnSub_ID"] = ret.Obj.FnSub_ID;

                    SvcResult<TSTU_B_Class> ClassSum = AdminBll.Sel_Class("");
                    Session["count"] = ClassSum.Obj.count;
                    SvcResult<TSTU_B_Teacher> teacount = AdminBll.Sel_Tea();
                    Session["Subject_count"] = teacount.Obj.count;
                    //Session["id"] = ret.Obj.FnTea_ID;
                    //stuid = ret.Obj.FnTea_ID;
                    return Json(err);
                }
            }

            return View();

        }
        /// <summary>
        /// 忘记密码
        /// </summary>
        /// <returns></returns>
        public ActionResult ForgotPassword()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ForgotPassword(string account, int? type)
        {
            if (type == 0 || type == null)
            {
                return Content("请选择账号类型");
            }
            if (string.IsNullOrEmpty(account))
            {
                return Content("请输入账号");
            }
            SvcResult<TSTU_B_Student> ret = LoginBll.ForgotPassword(account, type);
            if (ret.Error==null)
            {
                Redirect("/Default/Login");
            }
            return Content(ret.Error);
        }
        public ActionResult Null404()
        {
            return View();
        }
        public ActionResult _Null404()
        {
            return View();
        }
    }
}