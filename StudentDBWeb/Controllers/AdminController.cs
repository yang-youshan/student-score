﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StudentBLL;
using StudentModel;

namespace StudentDBWeb.Controllers
{
    public class AdminController : Controller
    {
        int _currentPage = 1;
        int _pageSize = 7;
        // GET: Admin
        public ActionResult Index()
        {
            if (Session["Name"] != null)
            {
                ViewBag.Name = Session["Name"];
            }
            if (Session["FsCla_Name"] != null)
            {
                ViewBag.FsCla_Name = Session["FsCla_Name"];
            }
            if (Session["count"] != null)
            {
                ViewBag.count = Session["count"];
            }
            if (Session["Subject_count"] != null)
            {
                ViewBag.Subject_count = Session["Subject_count"];
            }
            if (Session["Name"] == null || Session["FsCla_Name"] == null || Session["count"] == null || Session["Subject_count"] == null)
            {
                return Redirect("/Admin/Null404");
            }
            return View();
        }
        /// <summary>
        /// 教师
        /// </summary>
        /// <returns></returns>
        public ActionResult Teacher(string name, int? size,int? page = 1)
        {
            ViewBag.teaname = name;
            if (Session["Name"] != null)
            {
                ViewBag.Name = Session["Name"];
            }
            if (Session["FsCla_Name"] != null)
            {
                ViewBag.FsCla_Name = Session["FsCla_Name"];
            }
            if (Session["count"] != null)
            {
                ViewBag.count = Session["count"];
            }
            if (Session["Subject_count"] != null)
            {
                ViewBag.Subject_count = Session["Subject_count"];
            }
            if (Session["Name"] == null || Session["FsCla_Name"] == null || Session["count"] == null || Session["Subject_count"] == null)
            {
                return Redirect("/Admin/Null404");
            }

            SvcResult<TSTU_B_Teacher> tea = AdminBll.Sel_Teacher(name);
            ViewBag.tea = tea.ObjList;
            //分页
            _currentPage = page.HasValue ? page.Value : _currentPage;
            _pageSize = size.HasValue ? size.Value : _pageSize;
            var model = tea.ObjList.ToPagedList(_currentPage, _pageSize);
            ViewBag.PageCount = Math.Ceiling((double)model.TotalItemCount / (double)model.PageSize);
            ViewBag.size = _pageSize;
            return View(model);
        }
        /// <summary>
        /// 教师模糊查询
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public PartialViewResult Tea_PartialPage(string name, int? size, int? page = 1)
        {
            SvcResult<TSTU_B_Teacher> Result = AdminBll.Sel_Teacher(name);
            //分页
            _currentPage = page.HasValue ? page.Value : _currentPage;
            _pageSize = size.HasValue ? size.Value : _pageSize;
            var model = Result.ObjList.ToPagedList(_currentPage, _pageSize);
            return PartialView("Tea_PartialPage",model);
        }
        /// <summary>
        /// 删除教师
        /// </summary>
        /// <param name="id">教师编号</param>
        /// <returns></returns>
        public ActionResult DeleteConfirmed(int id)
        {
            SvcResult<TSTU_B_Teacher> ret = AdminBll.Teacher_Del(id);
            //return RedirectToAction("Index");
            return Content(ret.ErrorCode.ToString());
        }
        /// <summary>
        /// 新增教师
        /// </summary>
        /// <returns></returns>
        public ActionResult Teacher_Add(int? id)
        {
            if (Session["Name"] != null)
            {
                ViewBag.Name = Session["Name"];
            }
            if (Session["FsCla_Name"] != null)
            {
                ViewBag.FsCla_Name = Session["FsCla_Name"];
            }
            if (Session["count"] != null)
            {
                ViewBag.count = Session["count"];
            }
            if (Session["Subject_count"] != null)
            {
                ViewBag.Subject_count = Session["Subject_count"];
            }
            if (Session["Name"] == null || Session["FsCla_Name"] == null || Session["count"] == null || Session["Subject_count"] == null)
            {
                return Redirect("/Admin/Null404");
            }
            if (id != null)
            {
                SvcResult<TSTU_B_Teacher> tea = AdminBll.Sel_Teacher((int)id);
                ViewBag.tea = tea.ObjList;
                Session["teaid"] = id;
            }
            SvcResult<TSTU_B_Class> cla = TeacherBll.Sel_Class();
            ViewBag.cla = cla.ObjList;
            SvcResult<TSTU_B_Subject> sub = TeacherBll.Sel_Subject();
            ViewBag.sub = sub.ObjList;
            return View();
        }
        [HttpPost]
        public ActionResult Teacher_Add(int? id, string FsStu_Account, string FsStu_Name, string FsStu_Psw, int FnCla_ID, int FnSub_ID)
        {
            if (string.IsNullOrWhiteSpace(FsStu_Account))
            {
                return Content("请输入账号");
            }
            if (string.IsNullOrWhiteSpace(FsStu_Name))
            {
                return Content("请输入姓名");
            }
            if (string.IsNullOrWhiteSpace(FsStu_Psw))
            {
                return Content("请输入密码");
            }
            if (Session["teaid"] != null)
            {
                id = int.Parse(Session["teaid"].ToString());
            }
            SvcResult<TSTU_B_Teacher> ret = AdminBll.Tea_Add((int)id, FnSub_ID, FnCla_ID, FsStu_Account, FsStu_Psw, FsStu_Name);
            if (ret.ErrorCode == 1)
            {
                ret.Error = "执行成功";
                Session["teaid"] = null;
                Session.Remove("teaid");
                return Content(ret.ErrorCode.ToString());
            }
            if (!string.IsNullOrWhiteSpace(ret.Error))
            {
                return Content(ret.Error);

            }
            return Content(ret.Error);
        }
        /// <summary>
        /// 科目
        /// </summary>
        /// <returns></returns>
        public ActionResult Subject(string name, int? size, int? page = 1)
        {
            ViewBag.subname = name;
            if (Session["Name"] != null)
            {
                ViewBag.Name = Session["Name"];
            }
            if (Session["FsCla_Name"] != null)
            {
                ViewBag.FsCla_Name = Session["FsCla_Name"];
            }
            if (Session["count"] != null)
            {
                ViewBag.count = Session["count"];
            }
            if (Session["Subject_count"] != null)
            {
                ViewBag.Subject_count = Session["Subject_count"];
            }
            if (Session["Name"] == null || Session["FsCla_Name"] == null || Session["count"] == null || Session["Subject_count"] == null)
            {
                return Redirect("/Admin/Null404");
            }

            SvcResult<TSTU_B_Subject> subList = AdminBll.Sel_Subject(name);
            ViewBag.subList = subList.ObjList;
            //分页
            _currentPage = page.HasValue ? page.Value : _currentPage;
            _pageSize = size.HasValue ? size.Value : _pageSize;
            var model = subList.ObjList.ToPagedList(_currentPage, _pageSize);
            ViewBag.PageCount = Math.Ceiling((double)model.TotalItemCount / (double)model.PageSize);
            ViewBag.size = _pageSize;
            return View(model);
        }
        /// <summary>
        /// 模糊查询科目
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public PartialViewResult Sub_PartialPage(string name, int? size, int? page = 1)
        {
            SvcResult<TSTU_B_Subject> Result = AdminBll.Sel_Subject(name);
            //分页
            _currentPage = page.HasValue ? page.Value : _currentPage;
            _pageSize = size.HasValue ? size.Value : _pageSize;
            var model = Result.ObjList.ToPagedList(_currentPage, _pageSize);
            return PartialView("Sub_PartialPage", model);
        }
        [HttpPost]
        public ActionResult Subject(int id)
        {
            SvcResult<TSTU_B_Subject> subList = AdminBll.Sel_Subject("");
            return Json(subList.ObjList);
        }
        /// <summary>
        /// 新增科目
        /// </summary>
        /// <param name="id">科目编号</param>
        /// <returns></returns>
        public ActionResult Subject_Add(int? id)
        {
            if (Session["Name"] != null)
            {
                ViewBag.Name = Session["Name"];
            }
            if (Session["FsCla_Name"] != null)
            {
                ViewBag.FsCla_Name = Session["FsCla_Name"];
            }
            if (Session["count"] != null)
            {
                ViewBag.count = Session["count"];
            }
            if (Session["Subject_count"] != null)
            {
                ViewBag.Subject_count = Session["Subject_count"];
            }
            if (Session["Name"] == null || Session["FsCla_Name"] == null || Session["count"] == null || Session["Subject_count"] == null)
            {
                return Redirect("/Admin/Null404");
            }
            if (id != null)
            {
                SvcResult<TSTU_B_Subject> sub = AdminBll.Sel_Subject((int)id);
                ViewBag.sub = sub.ObjList;
                Session["subid"] = id;
            }
            return View();
        }
        [HttpPost]
        public ActionResult Subject_Add(int? id, string FsSub_Name)
        {
            if (string.IsNullOrWhiteSpace(FsSub_Name))
            {
                return Content("请输入科目名称");
            }
            if (Session["subid"] != null)
            {
                id = int.Parse(Session["subid"].ToString());
            }
            SvcResult<TSTU_B_Teacher> ret = AdminBll.Subject_Add((int)id, FsSub_Name);
            if (!string.IsNullOrWhiteSpace(ret.Error))
            {
                return Content(ret.Error);
            }
            else
            {

                Session["stuid"] = null;
                Session.Remove("stuid");
                ret.ErrorCode = 1;
                return Content(ret.ErrorCode.ToString());
            }
        }
        /// <summary>
        /// 删除科目
        /// </summary>
        /// <param name="id">科目编号</param>
        /// <returns></returns>
        public ActionResult DeleteSubject(int id)
        {
            SvcResult<TSTU_B_Teacher> ret = AdminBll.Subject_Del(id);
            //return RedirectToAction("Index");
            return Content(ret.ErrorCode.ToString());
        }
        /// <summary>
        /// 班级
        /// </summary>
        /// <returns></returns>
        public ActionResult Class(string name, int? size, int? page = 1)
        {
            ViewBag.claname = name;
            if (Session["Name"] != null)
            {
                ViewBag.Name = Session["Name"];
            }
            if (Session["FsCla_Name"] != null)
            {
                ViewBag.FsCla_Name = Session["FsCla_Name"];
            }
            if (Session["count"] != null)
            {
                ViewBag.count = Session["count"];
            }
            if (Session["Subject_count"] != null)
            {
                ViewBag.Subject_count = Session["Subject_count"];
            }
            if (Session["Name"] == null || Session["FsCla_Name"] == null || Session["count"] == null || Session["Subject_count"] == null)
            {
                return Redirect("/Admin/Null404");
            }

            SvcResult<TSTU_B_Class> classList = AdminBll.Sel_Class(name);
            ViewBag.classList = classList.ObjList;
            //分页
            _currentPage = page.HasValue ? page.Value : _currentPage;
            _pageSize = size.HasValue ? size.Value : _pageSize;
            var model = classList.ObjList.ToPagedList(_currentPage, _pageSize);
            ViewBag.size = _pageSize;
            ViewBag.PageCount = Math.Ceiling((double)model.TotalItemCount / (double)model.PageSize);
            return View(model);
        }
        /// <summary>
        /// 班级模糊查询
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public PartialViewResult Class_PartialPage(string name, int? size, int? page = 1)
        {

            SvcResult<TSTU_B_Class> Result = AdminBll.Sel_Class(name);
            //分页
            _currentPage = page.HasValue ? page.Value : _currentPage;
            _pageSize = size.HasValue ? size.Value : _pageSize;
            var model = Result.ObjList.ToPagedList(_currentPage, _pageSize);
            return PartialView("Class_PartialPage", model);
        }
        /// <summary>
        /// 新增班级
        /// </summary>
        /// <param name="id">班级编号</param>
        /// <returns></returns>
        public ActionResult Class_Add(int? id)
        {
            if (Session["Name"] != null)
            {
                ViewBag.Name = Session["Name"];
            }
            if (Session["FsCla_Name"] != null)
            {
                ViewBag.FsCla_Name = Session["FsCla_Name"];
            }
            if (Session["count"] != null)
            {
                ViewBag.count = Session["count"];
            }
            if (Session["Subject_count"] != null)
            {
                ViewBag.Subject_count = Session["Subject_count"];
            }
            if (Session["Name"] == null || Session["FsCla_Name"] == null || Session["count"] == null || Session["Subject_count"] == null)
            {
                return Redirect("/Admin/Null404");
            }
            if (id != null)
            {
                SvcResult<TSTU_B_Class> cla = AdminBll.Sel_Class((int)id);
                ViewBag.cla = cla.ObjList;
                Session["claid"] = id;
            }
            return View();
        }
        [HttpPost]
        public ActionResult Class_Add(int? id, string FsCla_Name)
        {
            if (string.IsNullOrWhiteSpace(FsCla_Name))
            {
                return Content("请输入班级名称");
            }
            if (Session["claid"] != null)
            {
                id = int.Parse(Session["claid"].ToString());
            }
            SvcResult<TSTU_B_Class> ret = AdminBll.Class_Add((int)id, FsCla_Name);
            if (!string.IsNullOrWhiteSpace(ret.Error))
            {
                return Content(ret.Error);
            }
            if (!string.IsNullOrWhiteSpace(ret.Error))
            {
                return Content(ret.Error);
            }
            else
            {

                Session["stuid"] = null;
                Session.Remove("stuid");
                ret.ErrorCode = 1;
                return Content(ret.ErrorCode.ToString());
            }
        }
        /// <summary>
        /// 删除班级
        /// </summary>
        /// <param name="id">科目编号</param>
        /// <returns></returns>
        public ActionResult DeleteClass(int id)
        {
            SvcResult<TSTU_B_Class> ret = AdminBll.Class_Del(id);
            //return RedirectToAction("Index");
            return Content(ret.ErrorCode.ToString());
        }
        /// <summary>
        /// 学生
        /// </summary>
        /// <returns></returns>
        public ActionResult Student(string name, int? size, int? page = 1)
        {
            ViewBag.stuname = name;
            if (Session["Name"] != null)
            {
                ViewBag.Name = Session["Name"];
            }
            if (Session["FsCla_Name"] != null)
            {
                ViewBag.FsCla_Name = Session["FsCla_Name"];
            }
            if (Session["count"] != null)
            {
                ViewBag.count = Session["count"];
            }
            if (Session["Subject_count"] != null)
            {
                ViewBag.Subject_count = Session["Subject_count"];
            }
            if (Session["Name"] == null || Session["FsCla_Name"] == null || Session["count"] == null || Session["Subject_count"] == null)
            {
                return Redirect("/Admin/Null404");
            }

            SvcResult<TSTU_B_Student> stuList = AdminBll.Student(name);
            ViewBag.stuList = stuList.ObjList;
            //分页
            _currentPage = page.HasValue ? page.Value : _currentPage;
            _pageSize = size.HasValue ? size.Value : _pageSize;
            var model = stuList.ObjList.ToPagedList(_currentPage, _pageSize);
            ViewBag.PageCount = Math.Ceiling((double)model.TotalItemCount / (double)model.PageSize);
            ViewBag.size = _pageSize;
            return View(model);
        }
        /// <summary>
        /// 学生模糊查询
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public PartialViewResult Stu_PartialPage(string name, int? size, int? page = 1)
        {
            SvcResult<TSTU_B_Student> Result = AdminBll.Student(name);
            _currentPage = page.HasValue ? page.Value : _currentPage;
            _pageSize = size.HasValue ? size.Value : _pageSize;
            var model = Result.ObjList.ToPagedList(_currentPage, _pageSize);
            return PartialView("Stu_PartialPage", model);
        }
        /// <summary>
        /// 添加学生
        /// </summary>
        /// <returns></returns>
        public ActionResult Student_Add(int? id)
        {
            if (Session["Name"] != null)
            {
                ViewBag.Name = Session["Name"];
            }
            if (Session["FsCla_Name"] != null)
            {
                ViewBag.FsCla_Name = Session["FsCla_Name"];
            }
            if (Session["count"] != null)
            {
                ViewBag.count = Session["count"];
            }
            if (Session["Subject_count"] != null)
            {
                ViewBag.Subject_count = Session["Subject_count"];
            }
            if (Session["id"] != null)
            {
                ViewBag.id = Session["id"];
            }
            if (Session["Name"] == null || Session["FsCla_Name"] == null || Session["count"] == null || Session["Subject_count"] == null)
            {
                return Redirect("/Admin/Null404");
            }
            SvcResult<TSTU_B_Class> cla = TeacherBll.Sel_Class();
            ViewBag.cla = cla.ObjList;
            if (id != null)
            {
                SvcResult<TSTU_B_Student> stu = TeacherBll.SelStudent((int)id);
                ViewBag.stu = stu.ObjList;
                Session["stuid"] = id;
            }

            //return RedirectToAction("Index");

            return View();
        }
        [HttpPost]
        public ActionResult Student_Add(int? id, string FsStu_Account, string FsStu_Name, string FsStu_Psw, int? FnCla_ID)
        {
            SvcResult<TSTU_B_Student> ret = new SvcResult<TSTU_B_Student>();

            if (string.IsNullOrWhiteSpace(FsStu_Account))
            {
                ret.Error = "请输入账号";
                return Content(ret.Error);
            }
            if (string.IsNullOrWhiteSpace(FsStu_Psw))
            {
                ret.Error = "请输入密码";
                return Content(ret.Error);
            }
            if (string.IsNullOrWhiteSpace(FsStu_Name))
            {
                ret.Error = "请输入姓名";
                return Content(ret.Error);
            }

            //int FnCla_ID = int.Parse(Request.Form["FnCla_ID"]);
            //int FnCla_ID = 1;
            if (FnCla_ID == 0 || FnCla_ID == null)
            {
                return Content("请选择班级");
            }
            if (Session["stuid"] != null)
            {
                id = int.Parse(Session["stuid"].ToString());
            }
            ret = TeacherBll.StudentEdit((int)id, FsStu_Account, FsStu_Name, FsStu_Psw, (int)FnCla_ID);
            
            if (!string.IsNullOrWhiteSpace(ret.Error))
            {
                return Content(ret.Error);
            }
            else
            {
                
                Session["stuid"] = null;
                Session.Remove("stuid");
                ret.ErrorCode = 1;
                return Content(ret.ErrorCode.ToString());
            }

        }
        /// <summary>
        /// 删除学生
        /// </summary>
        /// <param name="id">学生编号</param>
        /// <returns></returns>
        public ActionResult DeleteStudent(int id)
        {
            SvcResult<TSTU_B_Student> ret = TeacherBll.StudentDel(id);
            //return RedirectToAction("Index");
            return Content(ret.ErrorCode.ToString());
        }
        /// <summary>
        /// 404
        /// </summary>
        /// <returns></returns>
        public ActionResult Null404()
        {
            return View();
        }
    }
}