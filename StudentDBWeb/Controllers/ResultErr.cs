﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentDBWeb.Controllers
{
    public class ResultErr
    {
        //提示信息
        public string Msg { get; set; }
        //状态 0失败   1成功
        public int Status { get; set; }
    }
}