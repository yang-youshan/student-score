﻿using Newtonsoft.Json;
using StudentBLL;
using StudentModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StudentDBWeb.Controllers
{
    public class TeacherController : Controller
    {
        int _currentPage = 1;
        int _pageSize = 7;
        private IList<TSTU_B_Student> _allProducts = new List<TSTU_B_Student>();
        ObservableCollection<TSTU_B_Student> stu = new ObservableCollection<TSTU_B_Student>();
        
        // GET: Teacher
        public ActionResult Index()
        {
            if (Session["Name"] != null)
            {
                ViewBag.Name = Session["Name"];
            }
            if (Session["FsCla_Name"] != null)
            {
                ViewBag.FsCla_Name = Session["FsCla_Name"];
            }
            if (Session["count"] != null)
            {
                ViewBag.count = Session["count"];
            }
            if (Session["Subject_count"] != null)
            {
                ViewBag.Subject_count = Session["Subject_count"];
            }
            if (Session["id"] != null)
            {
                ViewBag.id = Session["id"];
            }
            if (Session["Name"] == null || Session["FsCla_Name"] == null || Session["count"] == null || Session["Subject_count"] == null)
            {
                return Redirect("/Teacher/Null404");
            }
            return View();
        }
        /// <summary>
        /// 总分
        /// </summary>
        /// <returns></returns>
        public ActionResult Total(string name, int? size,int? page = 1)
        {
            ViewBag.stuname = name;
            if (Session["Name"] != null)
            {
                ViewBag.Name = Session["Name"];
            }
            if (Session["FsCla_Name"] != null)
            {
                ViewBag.FsCla_Name = Session["FsCla_Name"];
            }
            if (Session["count"] != null)
            {
                ViewBag.count = Session["count"];
            }
            if (Session["Subject_count"] != null)
            {
                ViewBag.Subject_count = Session["Subject_count"];
            }
            if (Session["id"] != null)
            {
                ViewBag.id = Session["id"];
            }
            if (Session["Name"] == null || Session["FsCla_Name"] == null || Session["count"] == null || Session["Subject_count"] == null)
            {
                return Redirect("/Teacher/Null404");
            }
            SvcResult<TSTU_B_Score> Result = LoginBll.Result(name);

            if (Result == null)
            {
                return Redirect("/Teacher/Null404");
            }
            //分页
            _currentPage = page.HasValue ? page.Value : _currentPage;
            _pageSize = size.HasValue ? size.Value : _pageSize;
            ViewBag.size = _pageSize;
            var model = Result.ObjList.ToPagedList(_currentPage, _pageSize);
            ViewBag.PageCount = Math.Ceiling((double)model.TotalItemCount / (double)model.PageSize);
            return View(model);
        }
        [HttpPost]
        public ActionResult Total(int id)
        {
            if (id == 0)
            {
                id = 92;
            }
            SvcResult<TSTU_B_Student> Personal = TeacherBll.Sum_detil(id);
            Session["Personal"] = Personal.ObjList;
            if (Session["Personal"] != null)
            {
                ViewBag.Personal = Session["Personal"];
                Personal.Error = "zzzzzzzz";
            }
            else
            {
                SvcResult<TSTU_B_Student> S = TeacherBll.Sum_detil(74);
                ViewBag.Personal = S.ObjList;
                Personal.Error = "错误";
            }
            return Json(Personal.ObjList);
        }
        /// <summary>
        /// 各科排名
        /// </summary>
        /// <returns></returns>
        public ActionResult Subject(int? id, int? size,int? page = 1)
        {
            ViewBag.subid = id;
            if (Session["Name"] != null)
            {
                ViewBag.Name = Session["Name"];
            }
            if (Session["FsCla_Name"] != null)
            {
                ViewBag.FsCla_Name = Session["FsCla_Name"];
            }
            if (Session["count"] != null)
            {
                ViewBag.count = Session["count"];
            }
            if (Session["Subject_count"] != null)
            {
                ViewBag.Subject_count = Session["Subject_count"];
            }
            if (Session["id"] != null)
            {
                ViewBag.id = Session["id"];
            }
            if (Session["Name"] == null || Session["FsCla_Name"] == null || Session["count"] == null || Session["Subject_count"] == null)
            {
                return Redirect("/Teacher/Null404");
            }
            SvcResult<TSTU_B_Subject> Sub = TeacherBll.Sel_Subject();
            ViewBag.sub = Sub.ObjList;
            SvcResult<TSTU_B_Student> Subject_Result = LoginBll.Subject_Result(id);
            //分页
            _currentPage = page.HasValue ? page.Value : _currentPage;
            _pageSize = size.HasValue ? size.Value : _pageSize;
            ViewBag.size = _pageSize;
            var model = Subject_Result.ObjList.ToPagedList(_currentPage, _pageSize);
            ViewBag.PageCount = Math.Ceiling((double)model.TotalItemCount / (double)model.PageSize);
            return View(model);
        }
        [HttpPost]
        public ActionResult Subject(int id)
        {
            SvcResult<TSTU_B_Subject> Sub = TeacherBll.Sel_Subject();
            ViewBag.sub = Sub.ObjList;
            SvcResult<TSTU_B_Student> Subject_Result = LoginBll.Subject_Result(id);
            if (Subject_Result == null)
            {
                return Redirect("/Default/Null404");
            }
            ViewBag.Subject = Subject_Result.ObjList;
            return Json(Subject_Result.ObjList);
        }
        /// <summary>
        /// 学生窗口
        /// </summary>
        /// <returns></returns>
        public ActionResult Student(string name,  int? size,int? page = 1)
        {

            ViewBag.stuname = name;
            if (Session["Name"] != null)
            {
                ViewBag.Name = Session["Name"];
            }
            if (Session["FsCla_Name"] != null)
            {
                ViewBag.FsCla_Name = Session["FsCla_Name"];
            }
            if (Session["count"] != null)
            {
                ViewBag.count = Session["count"];
            }
            if (Session["Subject_count"] != null)
            {
                ViewBag.Subject_count = Session["Subject_count"];
            }
            if (Session["id"] != null)
            {
                ViewBag.id = Session["id"];
            }
            if (Session["Name"] == null || Session["FsCla_Name"] == null || Session["count"] == null || Session["Subject_count"] == null)
            {
                return Redirect("/Teacher/Null404");
            }
            if (Session["FnCla_ID"] == null)
            {
                return Redirect("/Default/Null404");
            }
            if (Session["FnSub_ID"] == null)
            {
                return Redirect("/Default/Null404");
            }
            SvcResult<TSTU_B_Student> student = TeacherBll.Student(int.Parse(Session["FnCla_ID"].ToString()), name);
            if (student == null)
            {
                return Redirect("/Default/Null404");
            }
            SvcResult<TSTU_B_Class> CLASS = TeacherBll.Sel_Class();
            ViewBag.CLASS = CLASS.ObjList;
            //分页
            _currentPage = page.HasValue ? page.Value : _currentPage;
            _pageSize = size.HasValue ? size.Value : _pageSize;
            ViewBag.size = _pageSize;
            _allProducts = student.ObjList;
            var model = _allProducts.ToPagedList(_currentPage, _pageSize);
            ViewBag.PageCount = Math.Ceiling((double)model.TotalItemCount / (double)model.PageSize);
            //foreach (var i in student.ObjList)
            //{
            //    TSTU_B_Student s = new TSTU_B_Student();
            //    s.FnStu_ID = i.FnStu_ID;
            //    s.rownumber = i.rownumber;
            //    s.FsCla_Name = i.FsCla_Name;
            //    s.FsStu_Name = i.FsStu_Name;
            //    s.FsStu_Account = i.FsStu_Account;
            //    stu.Add(s);
            //}
            return View(model);
        }
        /// <summary>
        /// 学生模糊查询
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        
        [HttpPost]
        public ActionResult Student(int id)
        {
            SvcResult<TSTU_B_Student> student = TeacherBll.Student(int.Parse(Session["FnCla_ID"].ToString()), "");
            return Json(student.ObjList);
        }
        /// <summary>
        /// 添加学生
        /// </summary>
        /// <returns></returns>
        public ActionResult StudentAdd()
        {
            if (Session["Name"] != null)
            {
                ViewBag.Name = Session["Name"];
            }
            if (Session["FsCla_Name"] != null)
            {
                ViewBag.FsCla_Name = Session["FsCla_Name"];
            }
            if (Session["count"] != null)
            {
                ViewBag.count = Session["count"];
            }
            if (Session["Subject_count"] != null)
            {
                ViewBag.Subject_count = Session["Subject_count"];
            }
            if (Session["id"] != null)
            {
                ViewBag.id = Session["id"];
            }
            if (Session["FnCla_ID"] == null)
            {
                return Redirect("/Default/Null404");
            }
            if (Session["Name"] == null || Session["FsCla_Name"] == null || Session["count"] == null || Session["Subject_count"] == null)
            {
                return Redirect("/Teacher/Null404");
            }
            SvcResult<TSTU_B_Class> student = TeacherBll.Sel_Class();
            if (student == null)
            {
                return Redirect("/Default/Null404");
            }
            return View(student.ObjList);
        }
        [HttpPost]
        public ActionResult StudentAdd(int? id, string FsStu_Account, string FsStu_Name, string FsStu_Psw, int? FnCla_ID)
        {
            SvcResult<TSTU_B_Student> ret = new SvcResult<TSTU_B_Student>();
            if (string.IsNullOrWhiteSpace(FsStu_Account))
            {
                ret.Error = "请输入账号";
                return Content(ret.Error);
            }
            if (string.IsNullOrWhiteSpace(FsStu_Psw))
            {
                ret.Error = "请输入密码";
                return Content(ret.Error);
            }
            if (string.IsNullOrWhiteSpace(FsStu_Name))
            {
                ret.Error = "请输入姓名";
                return Content(ret.Error);
            }

            //int FnCla_ID = int.Parse(Request.Form["FnCla_ID"]);
            //int FnCla_ID = 1;
            //if (string.IsNullOrWhiteSpace(FsStu_Account))
            //{
            //    ret.Error = "请输入账号";
            //    return Content(ret.Error);
            //}
            //if (string.IsNullOrWhiteSpace(FsStu_Psw))
            //{
            //    ret.Error = "请输入密码";
            //    return Content(ret.Error);
            //}
            //if (string.IsNullOrWhiteSpace(FsStu_Name))
            //{
            //    ret.Error = "请输入姓名";
            //    return Content(ret.Error);
            //}

            //int FnCla_ID = int.Parse(Request.Form["FnCla_ID"]);
            //int FnCla_ID = 1;
            if (FnCla_ID == 0 || FnCla_ID == null)
            {
                return Content("请选择班级");
            }
            ret = TeacherBll.StudentEdit(id, FsStu_Account, FsStu_Name, FsStu_Psw, (int)FnCla_ID);
            if (!string.IsNullOrWhiteSpace(ret.Error))
            {
                return Content(ret.Error);
            }
            else
            {
                ret.ErrorCode = 1;
                return Content(ret.ErrorCode.ToString());
            }
        }
        /// <summary>
        /// 录入分数
        /// </summary>
        /// <returns></returns>
        public ActionResult ScoreAdd(int? size,int? page = 1)
        {
            if (Session["Name"] != null)
            {
                ViewBag.Name = Session["Name"];
            }
            if (Session["FsCla_Name"] != null)
            {
                ViewBag.FsCla_Name = Session["FsCla_Name"];
            }
            if (Session["count"] != null)
            {
                ViewBag.count = Session["count"];
            }
            if (Session["Subject_count"] != null)
            {
                ViewBag.Subject_count = Session["Subject_count"];
            }
            if (Session["id"] != null)
            {
                ViewBag.id = Session["id"];
            }
            if (Session["FnCla_ID"] == null)
            {
                return Redirect("/Teacher/Null404");
            }
            if (Session["Name"] == null || Session["FsCla_Name"] == null || Session["count"] == null || Session["Subject_count"] == null)
            {
                return Redirect("/Teacher/Null404");
            }
            SvcResult<TSTU_B_Student> student = TeacherBll.Sel_score(int.Parse(Session["FnCla_ID"].ToString()), int.Parse(Session["FnSub_ID"].ToString()));
            ViewBag.stu = student.ObjList;
            if (student == null)
            {
                return Redirect("/Teacher/Null404");
            }
            _currentPage = page.HasValue ? page.Value : _currentPage;
            _pageSize = size.HasValue ? size.Value : _pageSize;
            ViewBag.size = _pageSize;
            var model = student.ObjList.ToPagedList(_currentPage, _pageSize);
            ViewBag.PageCount = Math.Ceiling((double)model.TotalItemCount / (double)model.PageSize);
            return View(model);
        }
        [HttpPost]
        public ActionResult ScoreAdd(int id, double? score)
        {
            SvcResult<TSTU_B_Student> student = TeacherBll.Sel_score(int.Parse(Session["FnCla_ID"].ToString()), int.Parse(Session["FnSub_ID"].ToString()));
            if (score==null||score==0)
            {
                return Content("请输入成绩");
            }
            if (score > 100 || score < 0)
            {
                ViewBag.score = student.ObjList;
                return Content("分数范围0-100");
            }
            SvcResult<TSTU_B_Score> s = TeacherBll.Score_Add((double)score, int.Parse(Session["FnSub_ID"].ToString()), id);
            return Json(student.ObjList);
        }
        /// <summary>
        /// 删除学生
        /// </summary>
        /// <param name="id">学生编号</param>
        /// <returns></returns>
        public ActionResult DeleteConfirmed(int id)
        {
            SvcResult<TSTU_B_Student> ret = TeacherBll.StudentDel(id);
            //return RedirectToAction("Index");
            stu.Remove(stu.FirstOrDefault(o => o.FnStu_ID == id));
            return Content(ret.ErrorCode.ToString());
        }
        /// <summary>
        /// 修改学生信息
        /// </summary>
        /// <param name="id">学生编号</param>
        /// <returns></returns>
        public ActionResult Edit(int? id)
        {
            Session["stuid"] = id;
            SvcResult<TSTU_B_Class> student = TeacherBll.Sel_Class();
            ViewBag.stu = student.ObjList;
            if (id == null)
            {
                return Redirect("/Teacher/Null404");
            }
            SvcResult<TSTU_B_Student> ret = TeacherBll.SelStudent((int)id);
            if (ret == null)
            {
                return Redirect("/Teacher/Null404");
            }
            //return RedirectToAction("Index");
            return View(ret.ObjList);
        }
        [HttpPost]
        public ActionResult Edit(string FsStu_Account, string FsStu_Name, string FsStu_Psw, int FnCla_ID)
        {
            int id = int.Parse(Session["stuid"].ToString());
            SvcResult<TSTU_B_Student> ret = TeacherBll.StudentEdit(id, FsStu_Account, FsStu_Name, FsStu_Psw, FnCla_ID);
            if (string.IsNullOrWhiteSpace(FsStu_Account))
            {
                ret.Error = "请输入账号";
                return Content(ret.Error);
            }
            if (string.IsNullOrWhiteSpace(FsStu_Psw))
            {
                ret.Error = "请输入密码";
                return Content(ret.Error);
            }
            if (string.IsNullOrWhiteSpace(FsStu_Name))
            {
                ret.Error = "请输入姓名";
                return Content(ret.Error);
            }
            if (FnCla_ID == 0)
            {
                return Content("请选择班级");
            }
          
            if (!string.IsNullOrWhiteSpace(ret.Error))
            {
                return Content(ret.Error);
            }
            else
            {
                ret.ErrorCode = 1;
                return Content(ret.ErrorCode.ToString());
            }
        }
        public ActionResult Null404()
        {
            return View();
        }
    }
}